<?php namespace Floorplan\Forms;

/**
 * Class SearchFormValidator
 *
 * @package Floorplan\Forms
 */
class SearchForm extends FormValidator {

    /**
     * Validation rules for searchbar
     *
     * @var array
     */
    protected $rules = [
        'search_query' => 'required|min:2'
    ];
}
