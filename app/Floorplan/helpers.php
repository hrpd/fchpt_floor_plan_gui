<?php

/**
 * @return string
 */
function getLocalePathPrepend()
{
    if (Lang::getLocale() === 'en') {
        $path = '/en/';
    } else {
        $path = '/';
    }

    return $path;
}

/**
 * @return string
 */
function getLocaleInvertedPath()
{
    if (Lang::getLocale() === 'en') {
        $path = '/' . substr(\Request::path(), 3);
    } else {
        $path = '/en/' . \Request::path();
    }

    return $path;
}

/**
 * @return string
 */
function getLocaleInvertedCaps()
{
    $rtn = '';

    if (Lang::getLocale() === 'en') {
        $rtn = 'SK';
    } else {
        $rtn = 'EN';
    }

    return $rtn;
}


/**
 * @param $a
 * @param $b
 *
 * @return int
 */
function sortEvents($a, $b)
{
    if ($a['from'] == $b['from']) {
        return 0;
    }

    return ($a['from'] < $b['from']) ? - 1 : 1;
}
