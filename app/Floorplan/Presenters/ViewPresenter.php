<?php namespace Floorplan\Presenters;

use \Floorplan\Repositories\RoomRepositoryInterface;


/**
 * Class ViewPresenter
 *
 * @package Floorplan\Presenters
 */
class ViewPresenter {

    /**
     * @var \Floorplan\Repositories\RoomRepositoryInterface
     */
    protected $roomRepo;

    /**
     * @var string
     */
    protected $localePrefix;

    /**
     * @param RoomRepositoryInterface $roomRepo
     */
    function __construct(RoomRepositoryInterface $roomRepo)
    {
        $this->roomRepo = $roomRepo;
        $this->localePrefix = getLocalePathPrepend();
    }

    /**
     * @param $sidebarData
     *
     * @return string
     */
    public function generateSidebar($sidebarData)
    {
        $sidebar = '<div class="panel-group" id="accordion">';

        foreach (json_decode($sidebarData) as $building_key => $building) {
            $sidebar .= '<div class="panel panel-default">';
            $sidebar .= $this->genPanelHeading('accordion', 'sidenav' . $building_key, ucfirst($this->roomRepo->translateBuildingIdLong($building_key)));
            $sidebar .= $this->genPanel_open('sidenav' . $building_key);

            $sidebar .= '<div class="panel-group" id="accordion-' . $building_key . '">';
            foreach ($building as $floor_key => $floor) {

                $sidebar .= '<div class="panel panel-default">';
                $sidebar .= $this->genPanelHeading('accordion-' . $building_key, 'sidenav-' . $building_key . '-' . $floor_key, ucwords(trans('rooms.floor')) . ' ' . ucwords($floor_key));
                $sidebar .= $this->genPanel_open('sidenav-' . $building_key . '-' . $floor_key);

                $sidebar .= $this->genList($floor, "p/" . $this->roomRepo->translateBuildingIdShort($building_key) . '-' . $floor_key);

                $sidebar .= $this->genPanel_close();
                $sidebar .= '</div>';
            }
            $sidebar .= '</div>';

            $sidebar .= $this->genPanel_close();
            $sidebar .= '</div>';
        }

        $sidebar .= '</div>';

        return $sidebar;
    }

    /**
     * @return array
     */
    public function getBuildingsNav()
    {
        $building_ids = $this->roomRepo->getBuildingNums();

        $building_links = [];

        foreach ($building_ids as $building_id) {
            $building_links[$building_id]["title"] = ucfirst($this->roomRepo->translateBuildingIdLong($building_id));
            $building_links[$building_id]["href"] = $this->localePrefix . 'p/' . $this->roomRepo->translateBuildingIdShort($building_id);
        }

        return $building_links;
    }

    /**
     * @param $building_id
     *
     * @return array
     */
    public function getFloorsNav($building_id)
    {
        $floor_ids = $this->roomRepo->getFloorSlugs($building_id);

        $floor_links = [];

        foreach ($floor_ids as $floor_id) {
            $floor_links[$floor_id]["title"] = ucfirst(trans('rooms.floor') . ' ' . $floor_id);
            $floor_links[$floor_id]["href"] = $this->localePrefix . 'p/' . $this->roomRepo->translateBuildingIdShort($building_id) . '-' . $floor_id;
        }

        return $floor_links;
    }

    /**
     * @param array $parts
     *
     * @return array|null
     */
    public function generateBreadcrumbs($parts = [])
    {
        $breadcrumbs = null;

        if (array_key_exists('building_id', $parts)) {
            $breadcrumbs[] = [
                'href'  => $this->localePrefix . 'p/' . $this->roomRepo->translateBuildingIdShort($parts['building_id']),
                'title' => ucfirst($this->roomRepo->translateBuildingIdLong($parts['building_id']))
            ];
        }

        if (array_key_exists('floor_id', $parts)) {
            $breadcrumbs[] = [
                'href'  => $this->localePrefix . 'p/' . $this->roomRepo->translateBuildingIdShort($parts['building_id']) . '-' . $parts['floor_id'],
                'title' => ucfirst(trans('rooms.floor') . ' ' . ucfirst($parts['floor_id']))
            ];
        }

        if (array_key_exists('room_id', $parts)) {
            $breadcrumbs[] = [
                'href'  => $this->localePrefix . 'p/' . $this->roomRepo->translateBuildingIdShort($parts['building_id']) . '-' . $parts['floor_id'] . '-' . $parts['room_id'],
                'title' => ucfirst(trans('rooms.room') . ' ' . ucfirst($parts['room_id']))
            ];
        }

        return $breadcrumbs;
    }


    /**
     * @param $dataParent
     * @param $href
     * @param $title
     *
     * @return string
     */
    private function genPanelHeading($dataParent, $href, $title)
    {
        $out = '<div class="panel-heading collapsed" data-toggle="collapse" data-parent="#' . $dataParent . '" href="#' . $href . '">';
        $out .= '<h4 class="panel-title">' . $title . '</h4>';
        $out .= '</div>';

        return $out;
    }

    /**
     * @param $id
     *
     * @return string
     */
    private function genPanel_open($id)
    {
        $out = '<div id="' . $id . '" class="panel-collapse collapse">';
        $out .= '<div class="panel-body">';

        return $out;
    }

    /**
     * @return string
     */
    private function genPanel_close()
    {
        $out = '</div>';
        $out .= '</div>';

        return $out;
    }

    /**
     * @param $rooms
     * @param $href_prefix
     *
     * @return string
     */
    private function genList($rooms, $href_prefix)
    {

        $out = '<ul class="nav nav-sidebar">';
        foreach ($rooms as $room) {
            $out .= '<li><a href="' . $this->localePrefix . $href_prefix . '-' . $room . '">' . ucwords(trans('rooms.room') . ' ' . $room) . '</a></li>';
        }
        $out .= '</ul>';

        return $out;
    }
}
