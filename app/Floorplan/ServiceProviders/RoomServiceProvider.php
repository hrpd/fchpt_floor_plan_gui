<?php namespace Floorplan\ServiceProviders;

use Illuminate\Support\ServiceProvider;

/**
 * Class RoomServiceProvider
 *
 * @package Floorplan\ServiceProviders
 */
class RoomServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $app->bind('Floorplan\Repositories\RoomRepositoryInterface', 'Floorplan\Repositories\APIRoomRepository');
    }
}
