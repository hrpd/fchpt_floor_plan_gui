<?php namespace Floorplan\Tools;

use File;

/**
 * Class PlanProcessor
 *
 * @package Floorplan\Tools
 */
class PlanProcessor extends PlanHelpers {

    /**
     * @var SimpleLogger
     */
    protected $simpleLogger;

    /**
     * @var string
     */
    protected $root;


    /**
     * @param null         $root
     * @param SimpleLogger $simpleLogger
     */
    function __construct($root = null, SimpleLogger $simpleLogger)
    {
        $this->root = $root ? : public_path();
        $this->sourceDir = $this->root . $this->sourceDir;
        $this->outputDir = $this->root . $this->outputDir;

        $this->simpleLogger = $simpleLogger;
    }

    /**
     * @return array
     */
    public function generateSVGFiles()
    {
        echo $this->simpleLogger->title('\Floorplan\Tools\PlanProcessor Log');

        $plans = File::directories($this->sourceDir);

        $created_files = [];

        foreach ($plans as $planpath) {

            $files = $this->getAllFilesInDirectory($planpath);
            $parts_to_process = $this->getFilesToFormSVG($files);
            $output = $this->svg_open();

            foreach ($this->chunks as $chunk) {
                $output .= $this->process($chunk, $parts_to_process[$chunk], $this->pattern[$chunk]);
            }

            $output .= $this->svg_close();

            $svg_filepath = $this->outputDir . basename($planpath) . '.svg';

            if (File::put($svg_filepath, $output)) {
                echo $this->simpleLogger->log('success', "$planpath");
                $created_files[] = $svg_filepath;
            } else {
                echo $this->simpleLogger->log('error', "$planpath");
                $created_files[] = null;
            }
        }

        return $created_files;
    }

    /**
     * @param $name
     * @param $path
     * @param $pattern
     *
     * @return string
     */
    private function process($name, $path, $pattern)
    {
//        get content of source .txt
        $source = File::get($path);

//        create array of elements, get parameters and form .svg
        $source = explode("\n", $source);

        $out = $this->group_open($name);

        foreach ($source as $line) {
            preg_match($pattern, $line, $matches);

            if ($matches) {
                $out .= $this->fill($name, $matches);
            }
        }

        $out .= $this->group_close();

        return $out;
    }

    /**
     * @return string
     */
    private function svg_open()
    {
        $rtn = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="plan_image">';

        $rtn .= '<g class="viewport">';

        return $rtn;
    }

    /**
     * @return string
     */
    private function svg_close()
    {
        $rtn = '</g>';
        $rtn .= '</svg>';

        return $rtn;
    }

    /**
     * @param $classname
     *
     * @return string
     */
    private function group_open($classname)
    {
        $rtn = '<g class="' . $classname . '">';

        return $rtn;
    }

    /**
     * @return string
     */
    private function group_close()
    {
        $rtn = '</g>';

        return $rtn;
    }

    /**
     * @param $part
     * @param $matches
     *
     * @return string
     */
    private function fill($part, $matches)
    {
        switch ($part) {
            case 'rooms':
                $content = $this->rooms_fill($matches);
                break;
            case 'miscs':
                $content = $this->miscs_fill($matches);
                break;
            case 'toilets':
                $content = $this->toilets_fill($matches);
                break;
            default:
                $content = $this->generic_fill($matches);
                break;
        }

        return $content;
    }

    /**
     * @param $matches
     *
     * @return string
     */
    private function rooms_fill($matches)
    {
        $id = $matches[1];
        $x = $matches[2];
        $y = $matches[3];
        $width = $matches[4];
        $height = $matches[5];

        $rtn = '<g class="room" id="' . $id . '">';
        $rtn .= '<rect class="room_wrap" width="' . $width . '" height="' . $height . '" x="' . $x . '" y="' . $y . '" />';
        $rtn .= '<rect class="room_occupancy" width="' . ($width - 2) . '" height="5" x="' . ($x + 1) . '" y="' . ($y + 1) . '" />';
        $rtn .= '<text class="room_no" x="' . $x . '" y="' . $y . '">null</text>';
        $rtn .= '<text class="room_type" x="' . $x . '" y="' . $y . '">null</text>';
        $rtn .= '</g>';

        return $rtn;
    }

    /**
     * @param $matches
     *
     * @return string
     */
    private function miscs_fill($matches)
    {
        $id = $matches[1];
        $x = $matches[2];
        $y = $matches[3];
        $width = $matches[4];
        $height = $matches[5];

        $rtn = '<g class="misc" id="' . $id . '">';
        $rtn .= '<rect class="room_wrap" width="' . $width . '" height="' . $height . '" x="' . $x . '" y="' . $y . '" />';
        $rtn .= '<text class="room_no" x="' . $x . '" y="' . $y . '">null</text>';
        $rtn .= '</g>';

        return $rtn;
    }

    /**
     * @param $matches
     *
     * @return string
     */
    private function toilets_fill($matches)
    {
        $id = $matches[1];
        $x = $matches[2];
        $y = $matches[3];
        $width = $matches[4];
        $height = $matches[5];

        $rtn = '<g class="toilet" id="' . $id . '">';
        $rtn .= '<rect class="room_wrap" width="' . $width . '" height="' . $height . '" x="' . $x . '" y="' . $y . '" />';
        $rtn .= '</g>';

        return $rtn;
    }

    /**
     * @param $matches
     *
     * @return mixed
     */
    private function generic_fill($matches)
    {
        return $matches[0];
    }

    /**
     * @param $files
     *
     * @return array
     */
    private function getFilesToFormSVG($files)
    {
        $parts_to_process = [];

        foreach ($files as $filepath) {

            $filename_clean = basename($filepath, '.txt');

            if (in_array($filename_clean, $this->chunks)) {
                $parts_to_process[$filename_clean] = $filepath;
            }
        }

        return $parts_to_process;
    }

    /**
     * @param $directory
     *
     * @return array
     */
    private function getAllFilesInDirectory($directory)
    {
        $files = scandir($directory, SCANDIR_SORT_ASCENDING);
        $output = [];
        foreach ($files as $file) {
            if (!in_array($file, ['.', '..'])) {
                $output[] = $directory . '/' . $file;
            }
        }

        return $output;
    }


}
