<?php namespace Floorplan\Tools;

/**
 * Class SimpleLogger
 *
 * @package Floorplan\Tools
 */
class SimpleLogger {

    /**
     * @param $title
     */
    public function title($title)
    {
        return "<h1>$title</h1>";
    }

    /**
     * @param string $type
     * @param        $message
     */
    public function log($type = '', $message)
    {

        $output = '<p>' . date('Y-m-d h:i:s') . ' - ';

        switch ($type) {
            case 'success':
                $output .= "<span style=\"color:green\">SUCCESS --> $message</span></p>";
                break;
            case 'error':
                $output .= "<span style=\"color:red\">ERROR --> $message</span></p>";
                break;
            default:
                $output .= "<span style=\"color:black\">INFO --> $message</span></p>";
                break;
        }

        return $output;
    }

}
