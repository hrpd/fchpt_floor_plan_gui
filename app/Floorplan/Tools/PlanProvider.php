<?php namespace Floorplan\Tools;

use File;

class PlanProvider extends PlanHelpers {

    /**
     * @var SimpleLogger
     */
    protected $simpleLogger;

    /**
     * @var string
     */
    protected $root;


    /**
     * @param null         $root
     * @param SimpleLogger $simpleLogger
     */
    function __construct($root = null, SimpleLogger $simpleLogger)
    {
        $this->root = $root ? : public_path();
        $this->sourceDir = $this->root . $this->sourceDir;
        $this->outputDir = $this->root . $this->outputDir;

        $this->simpleLogger = $simpleLogger;
    }

    /**
     * @return array
     */
    public function getRoomsData()
    {
        $filesToOpen = ['rooms', 'miscs', 'toilets'];

        $rtn['data'] = $this->scanPlanFoldersForData($filesToOpen);

        return $rtn;
    }

    /**
     * @param $filesToOpen
     *
     * @return array
     */
    private function scanPlanFoldersForData($filesToOpen)
    {
        $rtn = [];

        $dirs = File::directories($this->sourceDir);

        foreach ($dirs as $dirpath) {

            foreach ($filesToOpen as $filename) {

                $source = $this->getSourceDataArray($dirpath, $filename);

                foreach ($source as $line) {
                    preg_match($this->pattern['rooms'], $line, $matches);

                    if ($matches) {

                        $rtn[] = $this->getRoomDataArray($matches, $filename);
                    }
                }
            }
        }

        return $rtn;
    }

    /**
     * @param $matches
     * @param $filename
     *
     * @return array
     */
    private function getRoomDataArray($matches, $filename)
    {
        // remove leading 'r' from ID
        $roomData = explode('-', substr($matches[1], 1));
        $roomType = $this->getRoomType($filename);

        $out = [
            "id"       => (string) $matches[1],
            "aisName"  => (string) $this->getAisName($roomData, $roomType),
            "building" => (int) $roomData[0],
            "floor"    => (string) $roomData[1],
            "number"   => (string) $roomData[2],
            "roomType" => (int) $roomType,
        ];

        return $out;
    }

    /**
     * @param $filename
     *
     * @return int
     */
    private function getRoomType($filename)
    {
        $filename === 'toilets' ? $roomType = 4 : $roomType = 1;

        return $roomType;
    }

    /**
     * Returns room name in AIS format
     * eg. NB 601
     *
     * @param $roomData
     * @param $roomType
     *
     * @return string
     */
    private function getAisName($roomData, $roomType)
    {
        // Create Prefix
        $rtn = $roomData[0] == 1 ? 'NB ' : 'SB ';

        // If room return simple output
        if ($roomType == 1) {
            return $rtn . $roomData[2];
        }

        // Else return toilet version $building_slug T$building_id$floor_id$toiled_id ~ NB T611
        return $rtn . 'T' . $roomData[0] . $roomData[1] . $roomData[2];
    }

    /**
     * @param $dirpath
     * @param $filename
     *
     * @return array|string
     */
    private function getSourceDataArray($dirpath, $filename)
    {
        $source = File::get($dirpath . '/' . $filename . '.txt');
        $source = explode("\n", $source);

        return $source;
    }
}
