<?php namespace Floorplan\Tools;

/**
 * Class PlanHelpers
 *
 * @package Floorplan\Tools
 */
abstract class PlanHelpers {

    /**
     * @var string
     */
    protected $sourceDir = '/plans/source/';

    /**
     * @var string
     */
    protected $outputDir = '/plans/';

    /**
     * The order in which the final SVG is produced. A.K.A. keep top layers last!
     *
     * @var array
     */
    protected $chunks = ['background', 'toilets', 'stairs', 'elevators', 'miscs', 'rooms', 'icons'];

    /**
     * @var array
     */
    protected $pattern = [
        'background' => '/[\s\S]*/m',
        'elevators'  => '/[\s\S]*/',
        'icons'      => '/[\s\S]*/',
        'miscs'      => '/id="(\S+)" x="([0-9]+)" y="([0-9]+)" \S* width="([0-9]+)" height="([0-9]+)"/',
        'rooms'      => '/id="(\S+)" x="([0-9]+)" y="([0-9]+)" \S* width="([0-9]+)" height="([0-9]+)"/',
        'stairs'     => '/[\s\S]*/',
        'toilets'    => '/id="(\S+)" x="([0-9]+)" y="([0-9]+)" \S* width="([0-9]+)" height="([0-9]+)"/'
    ];

}
