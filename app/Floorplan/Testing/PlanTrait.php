<?php namespace Floorplan\Testing;

use Floorplan\Tools\SimpleLogger;
use Floorplan\Tools\PlanProcessor;
use Floorplan\Tools\PlanProvider;
use org\bovigo\vfs\VfsStream;

/**
 * Class PlanTrait
 *
 * @package Floorplan\Testing
 */
trait PlanTrait {

    private $planProcessor;
    private $planProvider;

    private $root;

    protected function _before()
    {
        $structure = [
            'plans' => [
                'source' => [
                    'b999f999' => [
                        'background.txt' => '<rect x="4" y="105" width="181" height="235"/>',
                        'elevators.txt'  => '<rect x="250" y="178" width="15" height="26"/>',
                        'icons.txt'      => '<rect x="250" y="178" width="15" height="26"/>',
                        'miscs.txt'      => '<rect id="r2-6-610" x="224" y="212" fill="#F7931E" width="18" height="32"/>',
                        'rooms.txt'      => '<rect id="r1-6-601" x="185" y="105" fill="#FF1D25" width="60" height="43"/>',
                        'stairs.txt'     => '<rect x="250" y="212" width="20" height="33"/>',
                        'toilets.txt'    => '<rect id="t1-6-1" x="281" y="209" fill="#FF00FF" width="16" height="25"/>'
                    ]
                ]
            ]
        ];

        $this->root = VfsStream::setup('virtual_root', null, $structure);
        $this->planProcessor = new PlanProcessor(VfsStream::url('virtual_root'), new SimpleLogger());
        $this->planProvider = new PlanProvider(VfsStream::url('virtual_root'), new SimpleLogger());
    }

    protected function _after()
    {
    }

}
