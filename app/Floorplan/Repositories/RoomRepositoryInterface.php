<?php namespace Floorplan\Repositories;


/**
 * Interface RoomRepositoryInterface
 *
 * @package Floorplan\Repositories
 */
interface RoomRepositoryInterface {

    /**
     * @return mixed
     */
    public function getSidebarData();

    /**
     * @return mixed
     */
    public function getBuildingNums();

    /**
     * @param $building_num
     *
     * @return mixed
     */
    public function getFloorSlugs($building_num);

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return mixed
     */
    public function getFloorDataForFloorplan($building_num, $floor_slug);

    /**
     * @param $room_id
     *
     * @return mixed
     */
    public function getRoomIDByNameInDB($room_id);

    /**
     * @param $room_id
     *
     * @return mixed
     */
    public function getRoomType($room_id);

    /**
     * @param $room_id
     *
     * @return mixed
     */
    public function getRoomTypeForFloorplan($room_id);

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return mixed
     */
    public function getRoomSlugs($building_num, $floor_slug);

    /**
     * @param $building_num
     * @param $floor_slug
     * @param $room_slug
     *
     * @return mixed
     */
    public function getSingleRoom($building_num, $floor_slug, $room_slug);

    /**
     * @param $room_data
     *
     * @return mixed
     */
    public function getPeopleInRoom($room_data);

    /**
     * @param $url
     *
     * @return mixed
     */
    public function getPaginatedData($url);

    /**
     * @return mixed
     */
    public function getWeekDates();

    /**
     * @param $date
     * @param $room_id
     *
     * @return mixed
     */
    public function getEventsForThisDate($date, $room_id);

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return mixed
     */
    public function getSingleFloor($building_num, $floor_slug);

    /**
     * @param $query
     *
     * @return mixed
     */
    public function getQueryMatches($query);

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return mixed
     */
    public function floorExists($building_num, $floor_slug);

    /**
     * @param $building_num
     * @param $floor_slug
     * @param $room_slug
     *
     * @return mixed
     */
    public function roomExists($building_num, $floor_slug, $room_slug);

    /**
     * @param $room
     *
     * @return mixed
     */
    public function isOccupied($room);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function translateBuildingIdLong($id);

    /**
     * @param $id
     *
     * @return mixed
     */
    public function translateBuildingIdShort($id);

    /**
     * @param $ais_id
     *
     * @return mixed
     */
    public function translateAisIDToHref($ais_id);
}
