<?php namespace Floorplan\Repositories;

/**
 * Class APIRoomRepository
 *
 * @package Floorplan\Repositories
 */
class APIRoomRepository implements RoomRepositoryInterface {

    /**
     * @var string
     */
    private $api_url = 'http://bc.durina.cc/api/v1';

    /**
     * Constructor
     */
    function __construct()
    {
        $this->api_url .= getLocalePathPrepend();
    }

    /**
     * @return string
     */
    public function getSidebarData()
    {

        $rtn = [];

        $buildings = $this->getBuildingNums();

        foreach ($buildings as $building) {
            $floors = $this->getFloorSlugs($building);

            foreach ($floors as $floor) {
                // sd - sidebar data
                $rtn[$building][$floor] = \Cache::rememberForever('sd_B' . $building . 'F' . $floor, function () use ($building, $floor) {
                    return $this->getRoomSlugs($building, $floor);
                });
//                $rtn[$building][$floor] = $this->getRoomSlugs($building, $floor);
            }
        }

        return json_encode($rtn);
    }

    /**
     * @return array
     */
    public function getBuildingNums()
    {
        // 1 -> new building
        // 2 -> old building
        return [1, 2];
    }

    /**
     * @param $building_num
     *
     * @return array
     */
    public function getFloorSlugs($building_num)
    {
        // New building has 7 Floors, Old has 6
        $building_num == 1 ? $top = 7 : $top = 6;

        // Append S1 for each building
        $rtn = ['s1'];

        foreach (range(0, $top) as $floor) {
            $rtn[] = (string) $floor;
        }

        return $rtn;
    }

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return array
     */
    public function getFloorDataForFloorplan($building_num, $floor_slug)
    {
        $url = $this->api_url . 'rooms?limit=50&building=' . $building_num . '&floor=' . $floor_slug . '&embed=todayEvent';

        $room_data = \Cache::remember('plan_data_B' . $building_num . 'F' . $floor_slug, $this->timeUntilMidnight(), function () use ($url) {
            return $this->getPaginatedData($url);
        });

        $res = [];

        foreach ($room_data as $i => $room) {

            $res[$room->id] = [
                'type'    => $room->roomType,
                'label'   => $this->getRoomTypeForFloorplan($room->roomType),
                'aisName' => $room->aisName,
            ];

            if ($room->roomType == 3) {
                $res[$room->id]['occupied'] = $this->isOccupied($room);
            }
        }

        return $res;

    }

    /**
     * @param $building_num
     * @param $floor_slug
     * @param $room_slug
     *
     * @return bool
     */
    public function roomExists($building_num, $floor_slug, $room_slug)
    {

        $room_data = $this->getSingleRoom($building_num, $floor_slug, $room_slug);

        // only if request problems
        if (!isset($room_data->meta->totalCount)) {
            return false;
        }

        return ($room_data->meta->totalCount == 1) ? true : false;
    }

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return bool
     */
    public function floorExists($building_num, $floor_slug)
    {

        $floors = $this->getFloorSlugs($building_num);

        if (in_array($floor_slug, $floors)) {
            return true;
        }

        return false;
    }

    /**
     * @param $building_num
     * @param $floor_slug
     * @param $room_slug
     *
     * @return array
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function getSingleRoom($building_num, $floor_slug, $room_slug)
    {
        $url = $this->api_url . 'rooms?limit=1&building=' . $building_num . '&floor=' . $floor_slug . '&number=' . $room_slug . '&embed=person,todayEvent,person.email,person.phone,person.detail,person.office,person.todayEvents,person.todayEvents.room,person.todayEvents.course,todayEvent.course,todayEvent.person,todayEvent.person.office';

        //srd - single room data
        $response = \Cache::remember('room_data_B' . $building_num . 'F' . $floor_slug . 'R' . $room_slug, $this->timeUntilMidnight(), function () use ($url) {
            return \Httpful\Request::get($url)->send();
        });

        $room_data = $response->body;

        if ($room_data->meta->totalCount == 0) {
            return false;
        }

        $rtn = [];

//        dd($room_data->data[0]->roomType);
        switch ($room_data->data[0]->roomType) {
            // office
            case 2:
                $rtn['room_type'] = $room_data->data[0]->roomType;
                $rtn['view_type'] = $this->getRoomType($room_data->data[0]->roomType);
                $rtn['people'] = $this->getPeopleInRoom($room_data->data[0]->person->data);
                break;

            // classroom
            case 3:
                $rtn['room_type'] = $room_data->data[0]->roomType;
                $rtn['view_type'] = $this->getRoomType($room_data->data[0]->roomType);
                $rtn['today_event'] = $this->getTodayEvents($room_data->data[0]->todayEvent->data);
                $rtn['week_dates'] = $this->getWeekDates();
                break;

            default:
                $rtn['room_type'] = $room_data->data[0]->roomType;
                $rtn['view_type'] = $this->getRoomType($room_data->data[0]->roomType);
        }

        return $rtn;
    }


    /**
     * @param $room_data
     *
     * @return array
     */
    public function getPeopleInRoom($room_data)
    {

        $people = [];

        foreach ($room_data as $i => $person) {
            $people[$i]['name'] = $this->getFullPersonName($person);
            $people[$i]['room'] = $this->loopThroughAndGet($person->office->data, 'aisName');
            $people[$i]['phone'] = $this->loopThroughAndGet($person->phone->data, 'phoneNumber');
            $people[$i]['email'] = $this->loopThroughAndGet($person->email->data, 'email');
            $people[$i]['details'] = $this->loopThroughAndGet($person->detail->data, 'text');
            $people[$i]['href'] = $person->aisId;
            $people[$i]['events'] = $this->getTodayEvents($person->todayEvents->data);
        }

        return $people;
    }

    /**
     * @param $data
     * @param $needle
     *
     * @return array
     */
    private function loopThroughAndGet($data, $needle)
    {
        $rtn = [];

        foreach ($data as $key => $item) {
            $rtn[$key] = $item->$needle;
        }

        return $rtn;
    }

    /**
     * @param $room
     *
     * @return bool
     */
    public function isOccupied($room)
    {
        $now = date('H:i:s');

        $occupied = false;

        foreach ($room->todayEvent->data as $event) {
            if (($event->starts < $now) && ($now < $event->ends)) {
                $occupied = $occupied || true;
            }
        }

        return $occupied;
    }

    /**
     * @param $url
     *
     * @return array
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function getPaginatedData($url)
    {
        $rtn = [];

        do {

            try {
                $response = \Httpful\Request::get($url)->send();
            } catch (\ErrorException $e) {
                $response = \Httpful\Request::get($url)->send();
            }

            foreach ($response->body->data as $entry) {
                $rtn[] = $entry;
            }

            $url = $response->body->meta->next;

        } while ($response->body->meta->next != "");

        sort($rtn);

        return $rtn;
    }

    /**
     * @return mixed
     */
    public function getWeekDates()
    {
        $timestampFirstDay = strtotime('last monday');
        if (date('Y-m-d', $timestampFirstDay) == date('Y-m-d', time() - 7 * 24 * 3600)) {
            // we are that day... => add one week
            $timestampFirstDay += 7 * 24 * 3600;
        }

        $currentDay = $timestampFirstDay;
        for ($i = 1; $i <= 5; $i ++) {
            $dates[$i] = date('Y-m-d', $currentDay);
            $currentDay += 24 * 3600;
        }

        return $dates;
    }

    /**
     * @param $room_id
     *
     * @return mixed
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function getRoomIDByNameInDB($room_id)
    {
        $url = $this->api_url . 'rooms?limit=1&name=' . $room_id;

        //srd - single room data
        $response = \Httpful\Request::get($url)->send();

        $room_data = $response->body->data[0];

        return $room_data->id;
    }

    /**
     * @param $date
     * @param $room_id
     *
     * @return array
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public function getEventsForThisDate($date, $room_id)
    {
        $url = $this->api_url . "rooms/$room_id/events?date=$date&embed=person,course";

        //srd - single room data
        $response = \Httpful\Request::get($url)->send();

        $room_data = $response->body->data;

        $events = $this->getTodayEvents($room_data);

        return $events;
    }

    /**
     * @param $events
     *
     * @return array
     */
    private function getTodayEvents($events)
    {
        $day = [];

        foreach ($events as $i => $event) {
            $day[$i]['date'] = $event->date;
            $day[$i]['from'] = $event->starts;
            $day[$i]['to'] = $event->ends;
            $day[$i]['lesson']['href'] = isset ($event->course->data[0]->aisId) ? $this->translateAisIDToHref($event->course->data[0]->aisId) : '';
            $day[$i]['lesson']['label'] = isset ($event->course->data[0]->name) ? $event->course->data[0]->name : '';
            $day[$i]['type'] = $this->translateRoomTypeToSlug($event->eventType);
            $day[$i]['lecturer']['label'] = isset ($event->person->data[0]->surname) ? $event->person->data[0]->surname : '';
            $day[$i]['lecturer']['href'] = isset ($event->person->data[0]->office->data[0]->id) ? $this->translateRoomIdToUri($event->person->data[0]->office->data[0]->id) : '';
            $day[$i]['room']['href'] = isset ($event->room->data[0]->id) ? $this->translateRoomIdToUri($event->room->data[0]->id) : '';
            $day[$i]['room']['label'] = isset ($event->room->data[0]->aisName) ? $event->room->data[0]->aisName : '';
        }

        usort($day, 'sortEvents');

        return $day;
    }

    /**
     * @param $ais_id
     *
     * @return string
     */
    public function translateAisIDToHref($ais_id)
    {
        return 'https://is.stuba.sk/auth/katalog/syllabus.pl?predmet=' . $ais_id;
    }

    /**
     * @param $type
     *
     * @return string
     */
    public function translateRoomTypeToSlug($type)
    {
        switch ($type) {
            case 1:
                return 'P';
                break;
            case 2:
                return 'C';
                break;
            case 3:
                return 'L';
                break;
        }

        return '';
    }


    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return mixed
     */
    public function getSingleFloor($building_num, $floor_slug)
    {
        $url = $this->api_url . 'rooms?limit=1&building=' . $building_num . '&floor=' . $floor_slug;

        //sfd - single floor data
        return \Cache::rememberForever('sfd_B' . $building_num . 'F' . $floor_slug, function () use ($url) {
            $response = \Httpful\Request::get($url)->send();
            return $response->body;
        });
    }

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return array
     */
    public function getRoomSlugs($building_num, $floor_slug)
    {

        $url = $this->api_url . 'rooms?limit=50&building=' . $building_num . '&floor=' . $floor_slug;

        $rtn = [];

        do {

            $response = \Httpful\Request::get($url)->send();

            foreach ($response->body->data as $room) {
                if ($this->isLegitRoom($room)) {
                    $rtn[] = (string) $room->number;
                }
            }

            $url = $response->body->meta->next;

        } while ($response->body->meta->next != "");

        sort($rtn);

        return $rtn;
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function getQueryMatches($query)
    {
        $url_r[0] = $this->api_url . 'rooms?aisNameLike=' . $query;
        $url_r[1] = $this->api_url . 'rooms?tagCloud=' . $query;
        $url_p[0] = $this->api_url . 'people?embed=office&name=' . $query;

        $rtn['rooms'] = $this->getRoomListMatchingQuery($url_r);
        $rtn['people'] = $this->getPeopleListMatchingQuery($url_p);

        return $rtn;
    }

    /**
     * @param $urls
     *
     * @return array
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    private function getRoomListMatchingQuery($urls)
    {
        $rtn = [];

        foreach ($urls as $url) {
            $response = \Httpful\Request::get($url)->send();

            foreach ($response->body->data as $i => $room) {
                $rtn[$i]['path'] = $this->translateRoomIdToUri($room->id);
                $rtn[$i]['label'] = $room->aisName;
            }
        }

        return $rtn;
    }

    /**
     * @param $urls
     *
     * @return array
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    private function getPeopleListMatchingQuery($urls)
    {
        $rtn = [];

        foreach ($urls as $url) {
            $response = \Httpful\Request::get($url)->send();

            foreach ($response->body->data as $i => $person) {
                $rtn[$i]['path'] = $this->translateRoomIdToUri($person->office->data[0]->id);
                $rtn[$i]['label'] = $this->getFullPersonName($person);
            }
        }

        return $rtn;
    }

    /**
     * @param $id
     *
     * @return string
     */
    private function translateRoomIdToUri($id)
    {
        return getLocalePathPrepend() . 'p/' . $this->translateBuildingIdShort(substr($id, 1)) . substr($id, 2);
    }


    /**
     * @param $id
     *
     * @return string
     */
    public function translateBuildingIdLong($id)
    {
        if ($id == 1) return trans('rooms.nb_long');

        return trans('rooms.ob_long');
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function translateBuildingIdShort($id)
    {
        if ($id == 1) return trans('rooms.nb_short');

        return trans('rooms.ob_short');
    }


    /**
     * @param $room_id
     *
     * @return string
     */
    public function getRoomType($room_id)
    {
        // "1 -unknownGeneric, 5-knownGeneric, 2-office, 3-classroom, 4-WC "

        $type = null;

        switch ((int) $room_id) {
            case 1 :
                $type = 'generic_room';
                break;
            case 2 :
                $type = 'office';
                break;
            case 3 :
                $type = 'classroom';
                break;
            case 4 :
                $type = 'generic_room';
                break;
            case 5 :
                $type = 'generic_room';
                break;
        }

        return $type;

    }

    /**
     * @param $room_id
     *
     * @return string
     */
    public function getRoomTypeForFloorplan($room_id)
    {
        // "1 -unknownGeneric, 5-knownGeneric, 2-office, 3-classroom, 4-WC "

        $type = null;

        switch ((int) $room_id) {
            case 2 :
                $type = trans('rooms.label_office');
                break;
            case 3 :
                $type = trans('rooms.label_classroom');
                break;
            default :
                $type = '';
                break;
        }

        return $type;

    }

    /**
     * @param $person
     *
     * @return string
     */
    private function getFullPersonName($person)
    {
        return $person->prefixTitle . ' ' . $person->name . ' ' . $person->surname . ' ' . $person->suffixTitle;
    }

    /**
     * @param $room
     *
     * @return bool
     */
    private function isLegitRoom($room)
    {
        return $room->id[0] === 'r';
    }

    /**
     * @return int
     */
    private function timeUntilMidnight()
    {
        return (int) ceil((strtotime('tomorrow midnight') - time()) / 60);
    }
}
