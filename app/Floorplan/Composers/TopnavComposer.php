<?php namespace Floorplan\Composers;

/**
 * Class TopnavComposer
 *
 * @package Floorplan\Composers
 */
class TopnavComposer {

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view->with([
            'switch_language_path' => getLocaleInvertedPath()
        ]);
    }

}
