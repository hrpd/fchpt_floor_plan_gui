<?php namespace Floorplan\Composers;

use Floorplan\Presenters\ViewPresenter;
use \Floorplan\Repositories\RoomRepositoryInterface;
use Laracasts\Utilities\JavaScript\Facades\JavaScript;


class RoomComposer extends HelperComposer {

    /**
     * @var \Floorplan\Presenters\ViewPresenter
     */
    protected $viewPresenter;

    /**
     * @var \Floorplan\Repositories\RoomRepositoryInterface
     */
    protected $roomRepo;


    /**
     * @param ViewPresenter           $viewPresenter
     * @param RoomRepositoryInterface $roomRepositoryInterface
     */
    function __construct(ViewPresenter $viewPresenter, RoomRepositoryInterface $roomRepositoryInterface)
    {
        $this->viewPresenter = $viewPresenter;
        $this->roomRepo = $roomRepositoryInterface;

    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view_data = $view->getData();

        $breadcumbParts = [
            'building_id' => $view_data['building_num'],
            'floor_id'    => $view_data['floor_slug'],
            'room_id'     => $view_data['room_slug']
        ];

        $plan_image = \File::get('plans/b' . $view_data['building_num'] . 'f' . $view_data['floor_slug'] . '.svg');

        Javascript::put([
            'locale_slug'    => \Lang::getLocale(),
            'locale_prefix'  => getLocalePathPrepend(),
            'focus_building' => $view_data['building_num'],
            'focus_floor'    => $view_data['floor_slug'],
            'focus_room_id'  => 'r' . $view_data['building_num'] . '-' . $view_data['floor_slug'] . '-' . $view_data['room_slug'],
            'draw_plan'      => 1,
        ]);

        $data_arr = [
            'focus_room_slug' => $view_data['room_slug'],
            'breadcrumbs'     => $this->viewPresenter->generateBreadcrumbs($breadcumbParts),
            'plan_image'      => $plan_image,
            'view_type'       => $view_data['room_data']['view_type']
        ];

        switch ($view_data['room_data']['room_type']) {
            case '2':
                $data_arr['people'] = $view_data['room_data']['people'];
                break;
            case '3':
                $data_arr['events'] = $view_data['room_data']['today_event'];
                $data_arr['week_dates'] = $view_data['room_data']['week_dates'];
                break;
        }

        $view->with($data_arr);
    }

}
