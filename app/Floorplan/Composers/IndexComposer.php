<?php namespace Floorplan\Composers;

use Floorplan\Presenters\ViewPresenter;

/**
 * Class IndexComposer
 *
 * @package Floorplan\Composers
 */
class IndexComposer {

    /**
     * @var \Floorplan\Presenters\ViewPresenter
     */
    protected $viewPresenter;


    /**
     * @param ViewPresenter $viewPresenter
     */
    function __construct(ViewPresenter $viewPresenter)
    {
        $this->viewPresenter = $viewPresenter;
    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view->with([
            'buildings'   => $this->viewPresenter->getBuildingsNav(),
            'breadcrumbs' => $this->viewPresenter->generateBreadcrumbs()
        ]);
    }

}
