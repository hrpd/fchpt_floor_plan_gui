<?php namespace Floorplan\Composers;

use Floorplan\Presenters\ViewPresenter;
use Floorplan\Repositories\RoomRepositoryInterface;

/**
 * Class SidebarComposer
 *
 * @package Floorplan\Composers
 */
class SidebarComposer {

    /**
     * @var \Floorplan\Presenters\ViewPresenter
     */
    private $viewPresenter;

    /**
     * @var \Floorplan\Repositories\RoomRepositoryInterface
     */
    private $roomRepo;

    /**
     * @param ViewPresenter           $viewPresenter
     * @param RoomRepositoryInterface $roomRepositoryInterface
     */
    function __construct(ViewPresenter $viewPresenter, RoomRepositoryInterface $roomRepositoryInterface)
    {
        $this->viewPresenter = $viewPresenter;
        $this->roomRepo = $roomRepositoryInterface;
    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view->with([
            'sidebar' => $this->viewPresenter->generateSidebar($this->roomRepo->getSidebarData())
        ]);
    }

}
