<?php namespace Floorplan\Composers;

use Floorplan\Presenters\ViewPresenter;
use \Floorplan\Repositories\RoomRepositoryInterface;
use Laracasts\Utilities\JavaScript\Facades\JavaScript;


/**
 * Class BuildingComposer
 *
 * @package Floorplan\Composers
 */
class FloorComposer extends HelperComposer {

    /**
     * @var \Floorplan\Presenters\ViewPresenter
     */
    protected $viewPresenter;

    /**
     * @var \Floorplan\Repositories\RoomRepositoryInterface
     */
    protected $roomRepo;


    /**
     * @param ViewPresenter           $viewPresenter
     * @param RoomRepositoryInterface $roomRepositoryInterface
     */
    function __construct(ViewPresenter $viewPresenter, RoomRepositoryInterface $roomRepositoryInterface)
    {
        $this->viewPresenter = $viewPresenter;
        $this->roomRepo = $roomRepositoryInterface;

    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view_data = $view->getData();

        $breadcumbParts = [
            'building_id' => $view_data['building_num'],
            'floor_id'    => $view_data['floor_slug'],
        ];

        $plan_image = \File::get('plans/b' . $view_data['building_num'] . 'f' . $view_data['floor_slug'] . '.svg');

        Javascript::put([
            'locale_slug'    => \Lang::getLocale(),
            'locale_prefix'  => getLocalePathPrepend(),
            'focus_building' => $view_data['building_num'],
            'focus_floor'    => $view_data['floor_slug'],
            'draw_plan'      => 1,
        ]);

        $view->with([
            'focus_floor' => $view_data['floor_slug'],
            'breadcrumbs' => $this->viewPresenter->generateBreadcrumbs($breadcumbParts),
            'plan_image'  => $plan_image,
            'view_type'   => 'generic_floor'
        ]);
    }

}
