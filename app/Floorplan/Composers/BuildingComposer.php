<?php namespace Floorplan\Composers;

use Floorplan\Presenters\ViewPresenter;

/**
 * Class BuildingComposer
 *
 * @package Floorplan\Composers
 */
class BuildingComposer extends HelperComposer {

    /**
     * @var \Floorplan\Presenters\ViewPresenter
     */
    protected $viewPresenter;


    /**
     * @param ViewPresenter $viewPresenter
     */
    function __construct(ViewPresenter $viewPresenter)
    {
        $this->viewPresenter = $viewPresenter;
    }

    /**
     * @param $view
     */
    public function compose($view)
    {
        $view_data = $view->getData();

        $building_num = $this->translateBuildingSlugToNum($view_data['building_slug']);

        $breadcumbParts = ['building_id' => $building_num];

        $view->with([
            'breadcrumbs' => $this->viewPresenter->generateBreadcrumbs($breadcumbParts),
            'floors'      => $this->viewPresenter->getFloorsNav($building_num)
        ]);
    }

}
