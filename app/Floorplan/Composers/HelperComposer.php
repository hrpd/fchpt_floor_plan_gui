<?php namespace Floorplan\Composers;

/**
 * Class HelperComposer
 *
 * @package Floorplan\Composers
 */
abstract class HelperComposer {

    /**
     * @param $building_slug
     *
     * @return int
     */
    protected function translateBuildingSlugToNum($building_slug)
    {
        $building_slug === 'nb' ? $building_num = '1' : $building_num = '2';

        return $building_num;
    }
}
