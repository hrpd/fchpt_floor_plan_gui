Feature: Search Bar
  In order to find a room
  As the site user
  I want to be able to find a room using search bar

  Scenario: User fills out search form correctly
    Given I am on "/"
    And I fill in "search_query" with "252"
    And I press "Odoslať"
    Then I should see "Miestnosti"
    And I should see "Zamestnanci"

  Scenario: User fills out search form with less than 2 characters
    Given I am on "/"
    And I fill in "search_query" with "c"
    And I press "Odoslať"
    Then I should see an "div.flash_message" element
