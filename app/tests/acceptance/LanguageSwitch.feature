Feature: Language Switch
  In order to change site language
  As the site user
  I want to be able to change it clicking on a link

  Scenario: User changes default language to EN
    Given I am on "/"
    And I follow "switch_language"
    Then I should see "SK"
    And I should be on "/en"

  Scenario: User changes /p/nb-6-675 page to EN
    Given I am on "/p/nb-6-675"
    And I follow "switch_language"
    Then I should see "SK"
    And I should be on "/en/p/nb-6-675"
