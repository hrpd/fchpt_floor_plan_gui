<?php
use Codeception\Util\Stub;
use Floorplan\Presenters\ViewPresenter;

/**
 * Class NavbarPresenterTest
 */
class NavbarPresenterTest extends \Codeception\TestCase\Test {

    /**
     * @var \CodeGuy
     */
    protected $codeGuy;

    protected $repoMock;
    protected $test_arr;

    protected function _before()
    {
        $this->test_arr = [
            1 => [
                1 => [123, 124],
                2 => [234, 235]
            ]
        ];

        $this->repoMock = Mockery::mock('Floorplan\Repositories\RoomRepositoryInterface');
        $this->repoMock->shouldReceive('translateBuildingIdShort')->andReturn('foo');
        $this->repoMock->shouldReceive('translateBuildingIdLong')->andReturn('foobar');
        $this->repoMock->shouldReceive('getBuildingNums')->andReturn([1, 2]);
        $this->repoMock->shouldReceive('getFloorSlugs')->andReturn([1, 2, 3]);
    }

    protected function _after()
    {
    }

    public function test_it_provides_correct_navigation_in_default_locale()
    {
        $presenter = new ViewPresenter($this->repoMock);
        $generated = $presenter->generateSidebar(json_encode($this->test_arr));

        $this->assertContains('<a href="/p/foo-1-124">Miestnosť 124</a>', $generated);
        $this->assertContains('<h4 class="panel-title">Poschodie 1</h4>', $generated);
        $this->assertContains('<h4 class="panel-title">Poschodie 2</h4>', $generated);
        $this->assertContains('<h4 class="panel-title">Foobar</h4>', $generated);

    }

    public function test_it_provides_correct_navigation_in_en_locale()
    {
        Lang::setLocale('en');
        $presenter = new ViewPresenter($this->repoMock);

        $generated = $presenter->generateSidebar(json_encode($this->test_arr));

        $this->assertContains('<a href="/en/p/foo-1-124">Room 124</a>', $generated);
        $this->assertContains('<h4 class="panel-title">Floor 1</h4>', $generated);
        $this->assertContains('<h4 class="panel-title">Foobar</h4>', $generated);

        Lang::setLocale('sk');
    }

    public function test_it_provides_data_for_buildings_navigation()
    {
        $presenter = new ViewPresenter($this->repoMock);
        $array_of_nav_data = $presenter->getBuildingsNav();

        $this->assertArrayHasKeys([1, 2], $array_of_nav_data);
        $this->assertArrayHasKeys(["href", "title"], $array_of_nav_data[1]);
        $this->assertEquals('Foobar', $array_of_nav_data[1]['title']);
        $this->assertEquals('/p/foo', $array_of_nav_data[1]['href']);
    }

    public function test_it_provides_data_for_floor_navigation()
    {
        $presenter = new ViewPresenter($this->repoMock);
        $array_of_nav_data = $presenter->getFloorsNav(1);

        $this->assertArrayHasKeys([1, 2, 3], $array_of_nav_data);
        $this->assertArrayHasKeys(["href", "title"], $array_of_nav_data[1]);
        $this->assertEquals('Poschodie 1', $array_of_nav_data[1]['title']);
        $this->assertEquals('/p/foo-1', $array_of_nav_data[1]['href']);
    }

    protected function assertArrayHasKeys($keys, $array)
    {
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }

}
