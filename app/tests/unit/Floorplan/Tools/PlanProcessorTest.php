<?php
use Codeception\Util\Stub;


/**
 * Class PlanProcessorTest
 */
class PlanProcessorTest extends \Codeception\TestCase\Test {

    /**
     * @var \CodeGuy
     */
    protected $codeGuy;

    use Floorplan\Testing\PlanTrait;

    function test_it_should_combine_provided_chunks_into_final_svg_and_return_path()
    {
        $this->assertEquals($this->planProcessor->generateSVGFiles(), ['vfs://virtual_root/plans/b999f999.svg']);
    }

    function test_it_should_combine_provided_chunks_into_final_svg_of_specific_format()
    {
        $expected_occurences = [
            'svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"',
            'id="plan_image"',
            'class="viewport"',
            'class="elevators"',
            'class="icons"',
            'class="miscs"',
            'class="rooms"',
            'class="stairs"',
            'class="toilets"',
        ];

        $array_of_created_svg_files = $this->planProcessor->generateSVGFiles();
        $this->assertContainsMultiple($expected_occurences, file_get_contents($array_of_created_svg_files[0]));
    }


    private function assertContainsMultiple($needles, $haystack)
    {
        foreach ($needles as $needle) {
            $this->assertContains($needle, $haystack);
        }
    }

}
