<?php
use Codeception\Util\Stub;

/**
 * Class SimpleLoggerTest
 */
class SimpleLoggerTest extends \Codeception\TestCase\Test {

    /**
     * @var \CodeGuy
     */
    protected $codeGuy;
    /**
     * @var
     */
    protected $simpleLogger;

    protected function _before()
    {
        $this->simpleLogger = new \Floorplan\Tools\SimpleLogger();
    }

    protected function _after()
    {
    }

    public function test_it_should_return_title()
    {
        $this->assertEquals($this->simpleLogger->title('Foo title'), '<h1>Foo title</h1>');
    }

    public function test_it_should_return_log_entry_of_type_success()
    {
        $log_entry = $this->simpleLogger->log('success', 'foobar');
        $this->assertContainsMultiple(['SUCCESS', 'green', 'foobar'], $log_entry);
    }

    public function test_it_should_return_log_entry_of_type_error()
    {
        $log_entry = $this->simpleLogger->log('error', 'foobar');
        $this->assertContainsMultiple(['ERROR', 'red', 'foobar'], $log_entry);
    }

    public function test_it_should_return_log_entry_of_generic_info_type()
    {
        $log_entry = $this->simpleLogger->log('', 'foobar');
        $this->assertContainsMultiple(['INFO', 'black', 'foobar'], $log_entry);
    }

    protected function assertContainsMultiple($items, $haystack)
    {
        foreach ($items as $item) {
            $this->assertContains($item, $haystack);
        }
    }

}
