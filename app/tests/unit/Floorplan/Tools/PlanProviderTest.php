<?php
use Codeception\Util\Stub;

/**
 * Class PlanProviderTest
 */
class PlanProviderTest extends \Codeception\TestCase\Test {

    /**
     * @var \CodeGuy
     */
    protected $codeGuy;

    use Floorplan\Testing\PlanTrait;

    function test_it_returns_array_of_room_data_based_on_content_of_files()
    {
        $expected_output = ["data" => [
            ["id"       => "r1-6-601",
             "aisName"  => "NB 601",
             "building" => 1,
             "floor"    => '6',
             "number"   => "601",
             "roomType" => 1],

            ["id"       => "r2-6-610",
             "aisName"  => "SB 610",
             "building" => 2,
             "floor"    => '6',
             "number"   => "610",
             "roomType" => 1],

            ["id"       => "t1-6-1",
             "aisName"  => "NB T161",
             "building" => 1,
             "floor"    => '6',
             "number"   => "1",
             "roomType" => 4]
        ]
        ];

        $returnedOutput = $this->planProvider->getRoomsData();

        $this->assertEquals($returnedOutput, $expected_output);
    }

}
