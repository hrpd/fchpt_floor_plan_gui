<?php
use Codeception\Util\Stub;

/**
 * Class HelpersTest
 */
class HelpersTest extends \Codeception\TestCase\Test {

    /**
     * @var \CodeGuy
     */
    protected $codeGuy;
    /**
     * @var
     */
    protected $request_path_from_this_test;

    protected function _before()
    {
        $this->request_path_from_this_test = '/';
    }

    protected function _after()
    {
    }

    // tests
    public function test_it_provides_correct_locale_paths_if_default_is_set()
    {
        $this->assertEquals('/', getLocalePathPrepend());
        $this->assertEquals('/en/' . $this->request_path_from_this_test, getLocaleInvertedPath());
        $this->assertEquals('EN', getLocaleInvertedCaps());
    }

    public function test_it_provides_correct_locale_paths_if_en_is_set()
    {
        Lang::setLocale('en');

        $this->assertEquals('/en/', getLocalePathPrepend());
        $this->assertEquals($this->request_path_from_this_test, getLocaleInvertedPath());
        $this->assertEquals('SK', getLocaleInvertedCaps());

        Lang::setLocale('sk');
    }

    public function test_it_can_sort_events_in_array_based_on_user_function()
    {
        $events = [
            0 => [
                'from' => '9:00'
            ],
            1 => [
                'from' => '7:00'
            ],
            2 => [
                'from' => '6:00'
            ],
            3 => [
                'from' => '6:00'
            ]
        ];

        $sorted = [
            0 => [
                'from' => '6:00'
            ],
            1 => [
                'from' => '6:00'
            ],
            2 => [
                'from' => '7:00'
            ],
            3 => [
                'from' => '9:00'
            ]
        ];

        usort($events, 'sortEvents');

        $this->assertEquals($sorted, $events);
    }
}
