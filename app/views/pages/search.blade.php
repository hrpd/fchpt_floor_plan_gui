@extends('master')

@section('content')

<h1 id="title_href" class="page_title">

</h1>
    <div class="portlet search">
        <div class="portlet_title">
            <h4>{{ trans('rooms.room_search_title') }}</h4>
        </div>
        <div class="portlet_body">
            <table class="table">
                <tbody>
                    @if(empty($found_rooms))
                    <tr>
                        <td class="empty">{{ trans('messages.no_entry') }}</td>
                    </tr>
                    @else
                        @foreach ($found_rooms as $room)
                        <tr>
                            <td>{{ link_to($room['path'], $room['label']) }}</td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    <div class="portlet search">
        <div class="portlet_title">
            <h4>{{ trans('rooms.people_search_title') }}</h4>
        </div>
        <div class="portlet_body">
            <table class="table">
                <tbody>
                    @if(empty($found_people))
                    <tr>
                        <td class="empty">{{ trans('messages.no_entry') }}</td>
                    </tr>
                    @else
                        @foreach ($found_people as $person)
                        <tr>
                            <td>{{ link_to($person['path'], $person['label']) }}</td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
