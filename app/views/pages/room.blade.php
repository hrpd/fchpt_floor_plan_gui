@extends('master')

@section('content')

    <h1 id="title_href" class="page_title">
    @if(isset($focus_room_slug))
        {{ trans('rooms.floor_h1') }}{{ ucfirst($focus_room_slug) }}
    @else
        {{ trans('rooms.floor_h1_without_focus_room') }}{{ ucfirst($focus_floor) }}
    @endif
    </h1>

    @include('partials.breadcrumbs')

    <div class="portlet details">
        @if($view_type == 'classroom')
            @include('api.classroom')
        @elseif($view_type == 'office')
            @include('api.office')
        @elseif($view_type == 'generic_room')
            @include('api.generic_room')
        @endif
    </div>

    @include('partials.plan')
@stop
