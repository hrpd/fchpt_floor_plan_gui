@extends('master')

@section('content')

    <h1 class="page_title">{{ trans('rooms.building_page_title') }}</h1>

    @include('partials.breadcrumbs')

    <ul class="list-group">
        @foreach ($floors as $floor)
            <li class="list-group-item">
                <a href="{{ $floor['href'] }}">{{ ucwords($floor['title']) }}</a>
            </li>
        @endforeach
    </ul>
@stop
