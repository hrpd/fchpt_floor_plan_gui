@extends('master')

@section('content')

    <h1 class="page_title">{{ trans('rooms.index_page_title') }}</h1>

<!--    @include('partials.breadcrumbs')-->

    <div class="links_list">
        @foreach ($buildings as $building)
            <div class="building_rect">
                <a href="{{ $building['href'] }}">{{ $building['title'] }}</a>
            </div>
        @endforeach
    </div>
@stop
