@if (Session::get('notification.message'))
<div class="flash_message {{ Session::get('notification.type') }}">
    <p>
        @if (Session::get('notification.type') == 'success')
        <i class="fa fa-check"></i>
        @else
        <i class="fa fa-exclamation"></i>
        @endif

        {{ Session::get('notification.message') }}

        <button type="button" class="close" aria-hidden="true">&times;</button>
    </p>
</div>
@endif
