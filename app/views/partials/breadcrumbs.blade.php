<ol class="breadcrumb">
    <li><a href="{{ getLocalePathPrepend() }}"><i class="fa fa-home"></i></a></li>
    @if ( ! is_null($breadcrumbs))
        @foreach ($breadcrumbs as $link)
            <li><a href="{{ $link['href'] }}">{{ $link['title'] }}</a></li>
        @endforeach
    @endif
</ol>
