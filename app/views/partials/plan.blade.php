<div id="plan_href" class="portlet plan">
    <div class="portlet_title">
        <h4>{{ trans('rooms.portlet_plan_title') }}
            <span>~ {{ ucfirst(trans('rooms.room')) }} <span id="mouseover_room"></span></span>
        </h4>

        <div class="portlet_controls" data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.tooltip_plan_movement') }}" >
            <button class="btn plan_controls" data-action="pan" data-valuex="50" data-valuey="0"><i class="fa fa-arrow-left"></i></button>
            <button class="btn plan_controls" data-action="pan" data-valuex="0" data-valuey="-50"><i class="fa fa-arrow-down"></i></button>
            <button class="btn plan_controls" data-action="pan" data-valuex="0" data-valuey="50"><i class="fa fa-arrow-up"></i></button>
            <button class="btn plan_controls" data-action="pan" data-valuex="-50" data-valuey="0"><i class="fa fa-arrow-right"></i></button>
            <button class="btn plan_controls" data-action="zoom" data-value="1.3"><i class="fa fa-plus"></i></button>
            <button class="btn plan_controls" data-action="zoom" data-value="0.7"><i class="fa fa-minus"></i></button>

            <a href="#title_href"><button class="btn">{{ trans('messages.label_href_details') }}</button></a>
        </div>
    </div>
    <div class="portlet_body">
        {{ $plan_image }}
    </div>
</div>
