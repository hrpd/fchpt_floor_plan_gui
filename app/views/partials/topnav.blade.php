<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://www.fchpt.stuba.sk/">
                {{ HTML::image('assets/images/fchpt_logo.svg', 'FCHPT Logo') }}
            </a>
        </div>

        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav navlinks">
                <li data-toggle="tooltip" data-placement="bottom" title="{{ trans('messages.tooltip_find') }}">
                    {{ Form::open(array('url' => getLocalePathPrepend().'s', 'method' => 'POST', 'class' => "navbar-form")) }}
                    {{ Form::text('search_query', null, ['id' => 'search_query', 'class' => 'form-control', 'required' => 'required', 'placeholder' => trans('rooms.input_placeholder') ]); }}
                    {{ Form::submit(trans('rooms.submit'), ['class' => 'btn',  'id' => 'search_submit']) }}
                    {{ Form::close() }}
                </li>
                <li class="navbuttons"><a href="{{ $switch_language_path }}" id="switch_language"><button class="btn">{{ getLocaleInvertedCaps() }}</button></a></li>
                <li class="navbuttons"><a href="http://hrcc.sk" target="_blank" id="credentials_href">
                        <svg version="1.1" id="hr_logo" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                             width="39px" height="24px" viewBox="0 0 39 24" enable-background="new 0 0 39 24" xml:space="preserve">
                            <g>
                                <polygon fill="#F7F7F7" points="16,9 4,9 4,6 0,6 0,5.304 0,8.92 0,20 4,20 4,13 16,13 16,18 19,18 19,8.92 19,5.304 19,0 16,0
                                    "></polygon>
                                <polygon fill="#F7F7F7" points="37.342,4 21,4 21,7 35,7 35,11 36.094,11 26.296,11 21,11 21,10.696 21,24 24,24 24,15 34,15
                                    34,19 38,19 38,10.696 38,13 39,13 39,2.937 39,4 	"></polygon>
                            </g>
                        </svg>
                </a></li>
            <ul>
        </div>
    </div>
</div>
