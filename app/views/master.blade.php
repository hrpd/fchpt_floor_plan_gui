<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>FCHPT - Floor plan</title>
        <meta name="description" content="Floor plan webapplication for FCHPT STU BA">
        <meta name="keywords" content="responsive, floor plan, interactive, fchpt, stu">
        <meta name="author" content="HR">

        {{ HTML::style('assets/main.min.css') }}
    </head>

    <body>
    @include('partials.topnav')
    @include('partials.message')

    <div class="container-fluid">
        <div class="row">
            <div id="sidebar">
                @include('partials.sidebar')
            </div>

            <div id="main">
                @yield('content')
            </div>
        </div>
    </div>

        {{ HTML::script('assets/main.min.js') }}

    </body>
</html>
