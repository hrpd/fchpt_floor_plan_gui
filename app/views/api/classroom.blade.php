<div class="portlet_title">
    <h4>{{ trans('rooms.classroom_details_title') }}</h4>

    <div class="portlet_controls">
        <a href="#plan_href"><button class="btn">{{ trans('messages.label_href_plan') }}</button></a>
    </div>
</div>

<div class="portlet_body">
    <div class="timetable">
        <ul class="nav nav-tabs">
            @foreach(range(1,5) as $day_no)
                <li id="timetable_nav_day-{{ $day_no }}"><a href="#day-{{ $day_no }}" class="timetable_nav_a" data-toggle="tab" data-eventsfor="{{ $week_dates[$day_no] }}">{{ trans('rooms.day-'.$day_no.'-label') }}</a></li>
            @endforeach
        </ul>

        <div id="classroom_timetable" class="tab-content">
            @foreach(range(1,5) as $day_no)
                <div class="tab-pane" id="day-{{ $day_no}}" ">
                    @include('api.timetable', ['events' => $events, 'display_lecturer' => true])
                </div>
            @endforeach
        </div>
    </div>
</div>
