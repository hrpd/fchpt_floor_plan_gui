<div class="portlet_title">
    <h4>{{ trans('rooms.office_details_title') }}</h4>

    <div class="portlet_controls">
        <a href="#plan_href"><button class="btn">{{ trans('messages.label_href_plan') }}</button></a>
    </div>
</div>

<div class="portlet_body">
    <ul class="details_person list-inline">
        @foreach ($people as $person)
        @if(count($people)>1)
        <li class="person col-lg-6">
        @else
        <li class="person col-lg-12">
        @endif
            <div class="profile">
                <a class="person_photo" href="http://is.stuba.sk/lide/clovek.pl?id={{ $person['href'] }}">
                    <img class="img-responsive" src="http://is.stuba.sk/lide/foto.pl?id={{ $person['href'] }}" alt="photo">
                </a>
            </div>
            <div class="person_body">
                <h4 class="person_name"><a href="http://is.stuba.sk/lide/clovek.pl?id={{ $person['href'] }}">{{ $person['name'] }}</a></h4>
                <hr/>
                @foreach ($person['room'] as $room)
                <p class="person_room"><a href="#plan_href">{{ $room }}</a></p>
                @endforeach
                <hr/>
                @foreach ($person['phone'] as $phone)
                <p class="person_phone"><a href="tel:{{ $phone }}">{{ $phone }}</a></p>
                @endforeach
                @foreach ($person['email'] as $email)
                <p class="person_mail"><a href="mailto:{{ $email }}" target="_blank">{{ $email }}</a></p>
                @endforeach
                <hr/>
            </div>
            @if( ! empty($person['events']))
            <p>{{ trans('messages.label_today_events') }}</p>
            <div class="timetable">
                @include('api.timetable', ['events' => $person['events'], 'display_room' => true])
            </div>
            @endif

            @if( ! empty($person['details']))
            <hr/>
            <ul class="person_details">
                @foreach ($person['details'] as $detail)
                <li>{{ $detail }}</li>
                @endforeach
            </ul>
            @endif
        </li>
        @endforeach
    </ul>
</div>
