<div class="table-responsive">
    <table>
        <tbody>
        @if(empty($events))
        <tr>
            <td class="empty">{{ trans('messages.no_entry') }}</td>
        </tr>
        @else
            @foreach($events as $event)
            <tr class="occupied">
                <td class="time">{{ $event['from'] }}</td>
                <td class="separator">-</td>
                <td class="time">{{ $event['to'] }}</td>
                <td class="type">{{ $event['type'] }}</td>
                <td class="lesson_title">{{ link_to($event['lesson']['href'], $event['lesson']['label']) }}</td>
                @if(isset($display_lecturer))
                <td class="lecturer">{{ link_to($event['lecturer']['href'], $event['lecturer']['label']) }}</td>
                @elseif(isset($display_room))
                <td class="classroom">{{ link_to($event['room']['href'], $event['room']['label']) }}</td>
                @endif
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
