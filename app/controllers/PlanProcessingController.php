<?php

use \Floorplan\Tools\PlanProcessor;

/**
 * Class PlanProcessingController
 */
class PlanProcessingController extends \BaseController {

    /**
     * @var Floorplan\Tools\PlanProcessor
     */
    protected $planProcessor;

    /**
     * @param PlanProcessor $planProvider
     */
    function __construct(PlanProcessor $planProvider)
    {
        $this->planProcessor = $planProvider;
    }

    /**
     *  Runs processing source txt -> svg
     */
    public function processRooms()
    {
        $this->planProcessor->generateSVGFiles();
    }


}
