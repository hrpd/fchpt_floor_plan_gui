<?php

use \Floorplan\Presenters\ViewPresenter;
use \Floorplan\Repositories\RoomRepositoryInterface;
use Laracasts\Utilities\JavaScript\Facades\JavaScript;

/**
 * Class PlanController
 */
class PlanController extends \BaseController {

    /**
     * @var Floorplan\Presenters\ViewPresenter
     */
    protected $viewPresenter;
    /**
     * @var Floorplan\Repositories\RoomRepositoryInterface
     */
    protected $roomRepo;

    /**
     * @var string
     */
    protected $sidebar;

    /**
     * @param ViewPresenter           $viewPresenter
     * @param RoomRepositoryInterface $roomRepositoryInterface
     */
    function __construct(ViewPresenter $viewPresenter, RoomRepositoryInterface $roomRepositoryInterface)
    {
        $this->viewPresenter = $viewPresenter;
        $this->roomRepo = $roomRepositoryInterface;

        $this->sidebar = $this->viewPresenter->generateSidebar($this->roomRepo->getSidebarData());
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
//        List of Buildings - old/new

        return View::make('pages.index');
    }

    /**
     * @param $building_slug
     */
    public function showBuilding($building_slug)
    {
//        List of floors S1-6|7
        return View::make('pages.building')->with([
            'building_slug' => $building_slug
        ]);
    }

    /**
     * @param $building_slug
     * @param $floor_slug
     *
     * @internal param $room_slug
     * @return \Illuminate\View\View
     */
    public function showFloor($building_slug, $floor_slug)
    {
        $building_num = $this->translateBuildingSlugToNum($building_slug);

        $floor_exists = $this->roomRepo->floorExists($building_num, $floor_slug);

        if (!$floor_exists) {
            return Redirect::to(getLocalePathPrepend() . "p/$building_slug")
                ->with(['notification.message' => trans('messages.error_floor')]);
        }

//      Plan of rooms on Floor $floor_id
        return View::make('pages.floor')->with([
            'building_num' => $building_num,
            'floor_slug'   => $floor_slug
        ]);
    }

    /**
     * @param $building_slug
     * @param $floor_slug
     * @param $room_slug
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showRoom($building_slug, $floor_slug, $room_slug)
    {
        $building_num = $this->translateBuildingSlugToNum($building_slug);

        $floor_exists = $this->roomRepo->floorExists($building_num, $floor_slug);

        if (!$floor_exists) {
            return Redirect::to(getLocalePathPrepend() . "p/$building_slug")
                ->with(['notification.message' => trans('messages.error_floor')]);
        }

        $room_data = $this->roomRepo->getSingleRoom($building_num, $floor_slug, $room_slug);

        if (!$room_data) {
            return Redirect::to(getLocalePathPrepend() . "p/$building_slug-$floor_slug")
                ->with(['notification.message' => trans('messages.error_room')]);
        }

        return View::make('pages.room')->with([
            'building_num' => $building_num,
            'floor_slug'   => $floor_slug,
            'room_slug'    => $room_slug,
            'room_data'    => $room_data
        ]);
    }

    /**
     * @param $building_slug
     *
     * @return int
     */
    private function translateBuildingSlugToNum($building_slug)
    {
        $building_slug === 'nb' ? $building_num = '1' : $building_num = '2';

        return $building_num;
    }
}
