<?php

use Floorplan\Repositories\RoomRepositoryInterface;
use Floorplan\Forms\FormValidationException;
use Floorplan\Forms\SearchForm;


/**
 * Class SearchController
 */
class SearchController extends \BaseController {

    /**
     * @var Floorplan\Repositories\RoomRepositoryInterface
     */
    protected $roomRepo;

    /**
     * @var Floorplan\Forms\SearchForm
     */
    protected $searchForm;

    /**
     * @param RoomRepositoryInterface $roomRepositoryInterface
     * @param SearchForm              $searchForm
     */
    function __construct(RoomRepositoryInterface $roomRepositoryInterface, SearchForm $searchForm)
    {
        $this->roomRepo = $roomRepositoryInterface;
        $this->searchForm = $searchForm;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function store()
    {
        $query = rawurlencode(Input::get('search_query'));

        try {
            $this->searchForm->validate(['search_query' => $query]);

            $found_data = $this->roomRepo->getQueryMatches($query);

            return View::make('pages.search')->with([
                'found_people' => $found_data['people'],
                'found_rooms'  => $found_data['rooms'],
            ]);


        } catch (FormValidationException $e) {
            return Redirect::back()->withInput()
                ->with(['notification.message' => trans('messages.error_search'),
                        'errors'               => $e->getErrors()]);
        }
    }

}
