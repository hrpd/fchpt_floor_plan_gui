<?php

use Floorplan\Repositories\RoomRepositoryInterface;

/**
 * Class ApiController
 */
class ApiController extends \BaseController {

    /**
     * @var Floorplan\Repositories\RoomRepositoryInterface
     */
    protected $roomRepo;

    /**
     * @param RoomRepositoryInterface $roomRepositoryInterface
     */
    function __construct(RoomRepositoryInterface $roomRepositoryInterface)
    {
        $this->roomRepo = $roomRepositoryInterface;
    }

    /**
     * @param $room_id
     *
     * @return \Illuminate\View\View
     */
    public function getRoomView($room_id)
    {
        return View::make('api.office');
    }

    /**
     * @param $building_num
     * @param $floor_slug
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomTypeAndState($building_num, $floor_slug)
    {

        $res = $this->roomRepo->getFloorDataForFloorplan($building_num, $floor_slug);

        return Response::json($res);
    }

    /**
     * @param $date
     * @param $room_id
     *
     * @return \Illuminate\View\View
     */
    public function getTimetableForDate($date, $room_id)
    {
        $events = $this->roomRepo->getEventsForThisDate($date, $room_id);

        return View::make('api.timetable')->with(compact('events'));
    }

    /**
     * @param $room_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomIDByNameInDB($room_id)
    {
        $res = $this->roomRepo->getRoomIDByNameInDB($room_id);

        return Response::json($res);
    }

}
