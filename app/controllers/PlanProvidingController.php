<?php

use \Floorplan\Tools\PlanProvider;


/**
 * Class PlanProvidingController
 */
class PlanProvidingController extends \BaseController {


    /**
     * @var Floorplan\Tools\PlanProvider
     */
    protected $planProvider;

    /**
     * @param PlanProvider $planProvider
     */
    function __construct(PlanProvider $planProvider)
    {
        $this->planProvider = $planProvider;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function provideRooms()
    {
        $roomsData = $this->planProvider->getRoomsData();

        return Response::json($roomsData);
    }


}
