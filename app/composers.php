<?php

View::composer('partials.sidebar','Floorplan\Composers\SidebarComposer');
View::composer('partials.topnav','Floorplan\Composers\TopnavComposer');
View::composer('pages.index', 'Floorplan\Composers\IndexComposer');
View::composer('pages.building', 'Floorplan\Composers\BuildingComposer');
View::composer('pages.floor', 'Floorplan\Composers\FloorComposer');
View::composer('pages.room', 'Floorplan\Composers\RoomComposer');
