<?php

$languages = array('en','sk');
$locale = Request::segment(1);

if (in_array($locale, $languages)) {
    \App::setLocale($locale);
} else{
    $locale = null;
}

Route::group(['prefix' => $locale], function()
{
    Route::pattern('building_slug', 'nb|ob|sb');
    Route::pattern('floor_slug', '[0-9]|s1|u');
    Route::pattern('room_slug', '[0-9a-zA-Z]+');

    Route::get('/', ['as' => 'home', 'uses' => 'PlanController@index' ]);
    Route::get('/p', function(){
        return Redirect::home();
    });

    // trailing hyphens are redirected in .htaccess
    Route::get('/p/{building_slug}', 'PlanController@showBuilding');
    Route::get('/p/{building_slug}-{floor_slug}', 'PlanController@showFloor');
    Route::get('/p/{building_slug}-{floor_slug}-{room_slug}', 'PlanController@showRoom');

    Route::post('/s', 'SearchController@store');
    Route::get('/s', function(){
        return Redirect::home();
    });

    Route::group(['prefix' => 'api'], function()
    {
        Route::pattern('room_id', '[0-9a-zA-Z-]+');
        Route::get('/getRoomView/{room_id}', 'ApiController@getRoomView');
        Route::get('/getRoomTypeAndState/{building_num}-{floor_slug}', 'ApiController@getRoomTypeAndState');
        Route::get('/getTimetableForDate/{date}/{room_id}', 'ApiController@getTimetableForDate');
        Route::get('/getRoomIDByNameInDB/{room_id}','ApiController@getRoomIDByNameInDB');
    });
});



// Plan Processing
Route::get('/process/rooms', 'PlanProcessingController@processRooms');
// Rooms Data
Route::get('/provide/rooms', 'PlanProvidingController@provideRooms');


