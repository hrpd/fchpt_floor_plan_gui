<?php

return array(

    'site_title' => 'FCHPT - Floor Plan',

    'floor' =>  'floor',
    'room'  =>  'room',
    'nb_long'  =>  'new building',
    'ob_long'  =>  'old building',
    'nb_short'  =>  'nb',
    'ob_short'  =>  'ob',

    'submit'  =>  'Submit',
    'input_placeholder'  =>  'provide room number or a few letters of a name (2 letters min): 252, mar, library, ...',
    'overlay_toggle'    =>  'Toggle Overlay',
    'overlay_label'     =>  'Overlay is active',
    'portlet_plan_title'=>  'Floor plan',
    'generic_room_label' => 'There are no details available.',
    'classroom_details_title' => 'Classroom timetable',
    'office_details_title' => 'Office occupancy',
    'floor_h1' => 'Details for room ',
    'floor_h1_without_focus_room' => 'Plan of floor ',

    'index_page_title' => 'Buildings listing',
    'building_page_title' => 'Floors listing',

    'day-1-label' => 'Monday',
    'day-2-label' => 'Tuesday',
    'day-3-label' => 'Wednesday',
    'day-4-label' => 'Thursday',
    'day-5-label' => 'Friday',

    'room_search_title' => 'Rooms',
    'people_search_title' => 'Employees',

    'label_office' => 'office',
    'label_classroom' => 'classroom',
);
