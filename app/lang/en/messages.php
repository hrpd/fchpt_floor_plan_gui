<?php

return array(

    'no_entry' => 'No entry has been found.',

    'label_today_events' => 'Today events:',

    'label_href_plan' => '#Plan',
    'label_href_details' => '#Details',

    'tooltip_find' => 'TIP: Press F while browsing the page to immediately jump into Find bar.',
    'tooltip_plan_movement' => 'TIP: Use WSAD/HJKL for panning and QZ for zooming.',

    'error_room' => 'That room does not exist!',
    'error_floor' => 'That floor does not exist!',
    'error_page' => 'That page does not exist!',
    'error_search' => 'Searching error! Please make sure that you entered at least two characters into search form.',
);
