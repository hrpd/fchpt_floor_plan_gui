<?php

return array(

    'no_entry' => 'Nepodarilo sa nájsť žiadny záznam.',

    'label_today_events' => 'Dnešné udalosti:',

    'label_href_plan' => '#Plán',
    'label_href_details' => '#Detaily',

    'tooltip_find' => 'TIP: Stlačte F počas prezerania stránky a okamžite sa dostanete do vyhľadávania.',
    'tooltip_plan_movement' => 'TIP: Použite WSAD/HJKL na posúvanie plánu a QZ na približovanie.',

    'error_room' => 'Požadovaná miestnosť neexistuje!',
    'error_floor' => 'Požadované poschodie neexistuje!',
    'error_page' => 'Požadovaná stránka neexistuje!',
    'error_search' => 'Vyhľadávanie nemohlo byť spracované! Uistite sa, že ste zadali aspoň 2 znaky.',
);
