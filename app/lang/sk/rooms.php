<?php

return array(

    'site_title' => 'FCHPT - Plán fakulty',

    'floor' => 'poschodie',
    'room'  =>  'miestnosť',
    'nb_long'  =>  'nová budova',
    'ob_long'  =>  'stará budova',
    'nb_short'  =>  'nb',
    'ob_short'  =>  'sb',

    'submit'  =>  'Odoslať',
    'input_placeholder'  =>  'zadajte číslo miestnosti alebo časť mena (min 2 znaky): 252, mar, kniznica, ...',
    'overlay_toggle'    =>  'Prepni prekryv',
    'overlay_label'     =>  'Prekryv je aktívny',
    'portlet_plan_title'=>  'Plán poschodia',
    'generic_room_label' => 'K tejto miestnosti nemáme dostupné nijaké údaje.',
    'classroom_details_title' => 'Rozvrh v miestnosti',
    'office_details_title' => 'Obsadenosť kancelárie',
    'floor_h1' => 'Detaily miestnosti ',
    'floor_h1_without_focus_room' => 'Plán poschodia ',

    'index_page_title' => 'Zoznam budov',
    'building_page_title' => 'Zoznam poschodí',

    'day-1-label' => 'Pondelok',
    'day-2-label' => 'Utorok',
    'day-3-label' => 'Streda',
    'day-4-label' => 'Štvrtok',
    'day-5-label' => 'Piatok',

    'room_search_title' => 'Miestnosti',
    'people_search_title' => 'Zamestnanci',

    'label_office' => 'kancelária',
    'label_classroom' => 'učebňa',
);
