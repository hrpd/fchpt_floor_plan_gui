<path fill="#2E3192" d="M774,236.48h3.46v-3.42h3.44v-3.45h3.49v-3.38h3.47v-3.36H793v1.71h-3.42v3.37h-3.47v3.42h-3.46v3.4h-3.48
        v3.43H774V236.48z"/>
    <g>
        <g id="g1581_3_">
            <path id="path1583_3_" fill="#93278F" d="M814.13,228.64v11.83h-1.11v-11.83H814.13z"/>
            <path id="path1585_3_" fill="#93278F" d="M813.57,234.55"/>
        </g>
        <g id="g1587_3_">
            <g id="g1589_3_">
                <path id="path1591_3_" fill="#93278F" d="M809.51,230.48c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    c-0.54,0-0.98,0.44-0.98,0.97C808.54,230.04,808.97,230.48,809.51,230.48z"/>
                <path id="path1593_3_" fill="#93278F" d="M809.51,229.51"/>
            </g>
            <path id="path1595_3_" fill="#93278F" d="M809.37,236.66v3.41c0,0.61-0.92,0.61-0.92,0l0-3.41h-1.22l1.3-4.52h-0.2l-0.77,2.58
                c-0.18,0.56-0.96,0.33-0.78-0.27l0.85-2.81c0.09-0.32,0.5-0.89,1.2-0.89h0.65l0,0h0.7c0.69,0,1.1,0.57,1.21,0.89l0.85,2.8
                c0.17,0.6-0.6,0.85-0.78,0.27l-0.77-2.57h-0.22l1.32,4.52h-1.22v3.42c0,0.61-0.92,0.61-0.92,0v-3.42H809.37z"/>
        </g>
        <g id="g1597_3_">
            <path id="path1599_3_" fill="#93278F" d="M816.47,230.76c-0.69,0-1.24,0.57-1.24,1.28v3.02c0,0.59,0.85,0.59,0.85,0v-2.76h0.2
                v7.64c0,0.78,1.13,0.76,1.13,0v-4.47h0.19v4.47c0,0.76,1.13,0.78,1.13,0v-7.64h0.2v2.76c0,0.59,0.84,0.59,0.84,0v-3
                c0-0.65-0.5-1.29-1.25-1.29L816.47,230.76z"/>
            <g id="g1601_3_">
                <path id="path1603_3_" fill="#93278F" d="M817.5,230.48c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    s-0.97,0.44-0.97,0.97C816.52,230.04,816.96,230.48,817.5,230.48z"/>
                <path id="path1605_3_" fill="#93278F" d="M817.5,229.51"/>
            </g>
        </g>
    </g>
    <path fill="#2E3192" d="M600.28,234.38h2.91v-2.88h2.89v-2.9h2.94v-2.85h2.92v-2.83h4.33v1.44h-2.88v2.84h-2.92v2.88h-2.92v2.87
        h-2.93v2.89h-4.35V234.38z"/>
    <g>
        <g id="g1581_2_">
            <path id="path1583_2_" fill="#93278F" d="M638.98,224.58v11.83h-1.11v-11.83H638.98z"/>
            <path id="path1585_2_" fill="#93278F" d="M638.42,230.5"/>
        </g>
        <g id="g1587_2_">
            <g id="g1589_2_">
                <path id="path1591_2_" fill="#93278F" d="M634.36,226.43c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    c-0.54,0-0.98,0.44-0.98,0.97C633.39,225.99,633.82,226.43,634.36,226.43z"/>
                <path id="path1593_2_" fill="#93278F" d="M634.36,225.45"/>
            </g>
            <path id="path1595_2_" fill="#93278F" d="M634.22,232.61v3.41c0,0.61-0.92,0.61-0.92,0l0-3.41h-1.22l1.3-4.52h-0.2l-0.77,2.58
                c-0.18,0.56-0.96,0.33-0.78-0.27l0.85-2.81c0.09-0.32,0.5-0.89,1.2-0.89h0.65l0,0h0.7c0.69,0,1.1,0.57,1.21,0.89l0.85,2.8
                c0.17,0.6-0.6,0.85-0.78,0.27l-0.77-2.57h-0.22l1.32,4.52h-1.22v3.42c0,0.61-0.92,0.61-0.92,0v-3.42H634.22z"/>
        </g>
        <g id="g1597_2_">
            <path id="path1599_2_" fill="#93278F" d="M641.32,226.7c-0.69,0-1.24,0.57-1.24,1.28v3.02c0,0.59,0.85,0.59,0.85,0v-2.76h0.2
                v7.64c0,0.78,1.13,0.76,1.13,0v-4.47h0.19v4.47c0,0.76,1.13,0.78,1.13,0v-7.64h0.2v2.76c0,0.59,0.84,0.59,0.84,0v-3
                c0-0.65-0.5-1.29-1.25-1.29L641.32,226.7z"/>
            <g id="g1601_2_">
                <path id="path1603_2_" fill="#93278F" d="M642.35,226.43c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    s-0.97,0.44-0.97,0.97C641.37,225.99,641.81,226.43,642.35,226.43z"/>
                <path id="path1605_2_" fill="#93278F" d="M642.35,225.45"/>
            </g>
        </g>
    </g>
    <path fill="#2E3192" d="M428,231.32h2.91v-2.88h2.89v-2.9h2.94v-2.85h2.92v-2.83H444v1.44h-2.88v2.84h-2.92v2.88h-2.92v2.87h-2.93
        v2.89H428V231.32z"/>
    <g>
        <g id="g1581_1_">
            <path id="path1583_1_" fill="#93278F" d="M467.87,224.48v11.83h-1.11v-11.83H467.87z"/>
            <path id="path1585_1_" fill="#93278F" d="M467.31,230.39"/>
        </g>
        <g id="g1587_1_">
            <g id="g1589_1_">
                <path id="path1591_1_" fill="#93278F" d="M463.25,226.32c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    c-0.54,0-0.98,0.44-0.98,0.97C462.27,225.88,462.71,226.32,463.25,226.32z"/>
                <path id="path1593_1_" fill="#93278F" d="M463.25,225.35"/>
            </g>
            <path id="path1595_1_" fill="#93278F" d="M463.11,232.5v3.41c0,0.61-0.92,0.61-0.92,0l0-3.41h-1.22l1.3-4.52h-0.2l-0.77,2.58
                c-0.18,0.56-0.96,0.33-0.78-0.27l0.85-2.81c0.09-0.32,0.5-0.89,1.2-0.89h0.65l0,0h0.7c0.69,0,1.1,0.57,1.21,0.89l0.85,2.8
                c0.17,0.6-0.6,0.85-0.78,0.27l-0.77-2.57h-0.22l1.32,4.52h-1.22v3.42c0,0.61-0.92,0.61-0.92,0v-3.42H463.11z"/>
        </g>
        <g id="g1597_1_">
            <path id="path1599_1_" fill="#93278F" d="M470.21,226.6c-0.69,0-1.24,0.57-1.24,1.28v3.02c0,0.59,0.85,0.59,0.85,0v-2.76h0.2
                v7.64c0,0.78,1.13,0.76,1.13,0v-4.47h0.19v4.47c0,0.76,1.13,0.78,1.13,0v-7.64h0.2v2.76c0,0.59,0.84,0.59,0.84,0v-3
                c0-0.65-0.5-1.29-1.25-1.29L470.21,226.6z"/>
            <g id="g1601_1_">
                <path id="path1603_1_" fill="#93278F" d="M471.24,226.32c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    s-0.97,0.44-0.97,0.97C470.26,225.88,470.7,226.32,471.24,226.32z"/>
                <path id="path1605_1_" fill="#93278F" d="M471.24,225.35"/>
            </g>
        </g>
    </g>
    <path fill="#2E3192" d="M252.42,236h2.91v-2.88h2.89v-2.9h2.94v-2.85h2.92v-2.83h4.33v1.44h-2.88v2.84h-2.92v2.88h-2.92v2.87h-2.93
        v2.89h-4.35V236z"/>
    <g>
        <g id="g1581_4_">
            <path id="path1583_4_" fill="#93278F" d="M290.87,225.1v11.83h-1.11V225.1H290.87z"/>
            <path id="path1585_4_" fill="#93278F" d="M290.31,231.02"/>
        </g>
        <g id="g1587_4_">
            <g id="g1589_4_">
                <path id="path1591_4_" fill="#93278F" d="M286.25,226.95c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    c-0.54,0-0.98,0.44-0.98,0.97C285.27,226.51,285.71,226.95,286.25,226.95z"/>
                <path id="path1593_4_" fill="#93278F" d="M286.25,225.97"/>
            </g>
            <path id="path1595_4_" fill="#93278F" d="M286.11,233.13v3.41c0,0.61-0.92,0.61-0.92,0l0-3.41h-1.22l1.3-4.52h-0.2l-0.77,2.58
                c-0.18,0.56-0.96,0.33-0.78-0.27l0.85-2.81c0.09-0.32,0.5-0.89,1.2-0.89h0.65l0,0h0.7c0.69,0,1.1,0.57,1.21,0.89l0.85,2.8
                c0.17,0.6-0.6,0.85-0.78,0.27l-0.77-2.57h-0.22l1.32,4.52h-1.22v3.42c0,0.61-0.92,0.61-0.92,0v-3.42H286.11z"/>
        </g>
        <g id="g1597_4_">
            <path id="path1599_4_" fill="#93278F" d="M293.21,227.22c-0.69,0-1.24,0.57-1.24,1.28v3.02c0,0.59,0.85,0.59,0.85,0v-2.76h0.2
                v7.64c0,0.78,1.13,0.76,1.13,0v-4.47h0.19v4.47c0,0.76,1.13,0.78,1.13,0v-7.64h0.2v2.76c0,0.59,0.84,0.59,0.84,0v-3
                c0-0.65-0.5-1.29-1.25-1.29L293.21,227.22z"/>
            <g id="g1601_4_">
                <path id="path1603_4_" fill="#93278F" d="M294.24,226.95c0.54,0,0.97-0.44,0.97-0.97c0-0.54-0.44-0.97-0.97-0.97
                    s-0.97,0.44-0.97,0.97C293.26,226.51,293.7,226.95,294.24,226.95z"/>
                <path id="path1605_4_" fill="#93278F" d="M294.24,225.97"/>
            </g>
        </g>
    </g>
    <g transform="scale(0.24,0.24)">
        <path id="path3261_1_" display="none" fill="#93278F" d="M1785.96,829.7v-36.67c-0.01-3.66,2.54-6.7,6.02-6.7h33.25
            c3.37,0,6.03,3,6.02,6.7v36.71c0.01,3.88-2.69,6.62-5.99,6.62h-33.29l0,0C1788.68,836.37,1785.94,833.46,1785.96,829.7"/>
        <path fill="#FFFFFF" d="M1783.33,787.5v50h50v-50H1783.33z M1825,829.17h-33.33v-33.33H1825V829.17z"/>
        <path id="path3265_1_" fill="#93278F" d="M1816.8,757.94c-0.04-4.79,6.24-4.85,6.28,0l-0.11,14.58l3.39-4.6
            c2.32-2.81,6.41,1.13,3.87,4.32l-8.17,10.78c-1.28,1.43-2.96,1.42-4.23,0l-8.18-10.82c-2.51-3.19,1.62-7.18,3.9-4.32l3.36,4.64
            L1816.8,757.94"/>
        <path id="path3267_1_" fill="#93278F" d="M1794.02,780.52c-0.04,4.79,6.24,4.84,6.28,0l-0.11-14.58l3.39,4.56
            c2.32,2.85,6.42-1.1,3.9-4.28l-8.21-10.78c-1.28-1.44-2.96-1.43-4.23,0l-8.18,10.82c-2.51,3.18,1.62,7.17,3.91,4.28l3.36-4.6
            L1794.02,780.52"/>
        <path id="path3269_1_" fill="#93278F" d="M1823.44,814c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0c-0.57,0-1.11-0.51-1.1-1.25
            v-8.81c-0.01-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814"/>
        <path id="path3271_1_" fill="#93278F" d="M1821.65,814.92h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V814.92"/>
        <path id="path3273_1_" fill="#93278F" d="M1818.44,814.92h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V814.92"/>
        <path id="path3275_1_" fill="#93278F" d="M1818.69,802.77c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.17-0.01-2.11,1.03-2.11,2.3C1816.58,801.72,1817.52,802.76,1818.69,802.77"/>
        <path id="path3277_1_" fill="#93278F" d="M1813.41,814c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0c-0.57,0-1.11-0.51-1.1-1.25
            v-8.81c-0.01-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814"/>
        <path id="path3279_1_" fill="#93278F" d="M1811.62,814.92h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V814.92"/>
        <path id="path3281_1_" fill="#93278F" d="M1808.4,814.92h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V814.92"/>
        <path id="path3283_1_" fill="#93278F" d="M1808.66,802.77c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.11,1.03-2.12,2.3C1806.55,801.72,1807.48,802.76,1808.66,802.77"/>
        <path id="path3285_1_" fill="#93278F" d="M1803.37,814c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0c-0.57,0-1.11-0.51-1.09-1.25
            v-8.81c-0.02-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814"/>
        <path id="path3287_1_" fill="#93278F" d="M1801.58,814.92h-2.81v11.95c0.01,2.11,2.81,2.11,2.81,0V814.92"/>
        <path id="path3289_1_" fill="#93278F" d="M1798.37,814.92h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V814.92"/>
        <path id="path3291_1_" fill="#93278F" d="M1798.62,802.77c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.12,1.03-2.12,2.3C1796.51,801.72,1797.45,802.76,1798.62,802.77"/>
    </g>
    <g transform="scale(0.24,0.24)">
        <path id="path3261_4_" display="none" fill="#93278F" d="M1048.5,829.7v-36.67c-0.01-3.66,2.53-6.7,6.02-6.7h33.25
            c3.37,0,6.03,3,6.02,6.7v36.71c0.01,3.88-2.69,6.62-5.99,6.62h-33.29l0,0C1051.23,836.37,1048.49,833.46,1048.5,829.7"/>
        <path fill="#FFFFFF" d="M1045.83,787.5v50h50v-50H1045.83z M1087.5,829.17h-33.33v-33.33h33.33V829.17z"/>
        <path id="path3265_4_" fill="#93278F" d="M1079.35,757.94c-0.04-4.79,6.24-4.85,6.28,0l-0.11,14.58l3.39-4.6
            c2.32-2.81,6.41,1.13,3.87,4.32l-8.17,10.78c-1.28,1.43-2.96,1.42-4.23,0l-8.18-10.82c-2.51-3.19,1.62-7.18,3.9-4.32l3.36,4.64
            L1079.35,757.94"/>
        <path id="path3267_4_" fill="#93278F" d="M1056.57,780.52c-0.04,4.79,6.24,4.84,6.28,0l-0.11-14.58l3.39,4.56
            c2.32,2.85,6.42-1.1,3.9-4.28l-8.21-10.78c-1.28-1.44-2.96-1.43-4.23,0l-8.18,10.82c-2.51,3.18,1.62,7.17,3.91,4.28l3.36-4.6
            L1056.57,780.52"/>
        <path id="path3269_4_" fill="#93278F" d="M1085.99,814c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0c-0.57,0-1.11-0.51-1.1-1.25
            v-8.81c-0.01-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814"/>
        <path id="path3271_4_" fill="#93278F" d="M1084.2,814.92h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V814.92"/>
        <path id="path3273_4_" fill="#93278F" d="M1080.99,814.92h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V814.92"/>
        <path id="path3275_4_" fill="#93278F" d="M1081.24,802.77c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.17-0.01-2.11,1.03-2.11,2.3C1079.13,801.72,1080.07,802.76,1081.24,802.77"/>
        <path id="path3277_4_" fill="#93278F" d="M1075.95,814c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0c-0.57,0-1.11-0.51-1.1-1.25
            v-8.81c-0.01-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814"/>
        <path id="path3279_4_" fill="#93278F" d="M1074.16,814.92h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V814.92"/>
        <path id="path3281_4_" fill="#93278F" d="M1070.95,814.92h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V814.92"/>
        <path id="path3283_4_" fill="#93278F" d="M1071.21,802.77c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.11,1.03-2.12,2.3C1069.09,801.72,1070.03,802.76,1071.21,802.77"/>
        <path id="path3285_4_" fill="#93278F" d="M1065.92,814c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0c-0.57,0-1.11-0.51-1.09-1.25
            v-8.81c-0.02-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814"/>
        <path id="path3287_4_" fill="#93278F" d="M1064.13,814.92h-2.81v11.95c0.01,2.11,2.81,2.11,2.81,0V814.92"/>
        <path id="path3289_4_" fill="#93278F" d="M1060.91,814.92h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V814.92"/>
        <path id="path3291_4_" fill="#93278F" d="M1061.17,802.77c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.12,1.03-2.12,2.3C1059.06,801.72,1059.99,802.76,1061.17,802.77"/>
    </g>
    <g transform="scale(0.24,0.24)">
        <path id="path3261_2_" display="none" fill="#93278F" d="M2499.1,831.99v-36.67c-0.01-3.66,2.53-6.7,6.02-6.7h33.25
            c3.37,0,6.02,3,6.02,6.7v36.71c0.01,3.88-2.69,6.62-5.99,6.62h-33.28l0,0C2501.82,838.66,2499.08,835.75,2499.1,831.99"/>
        <path fill="#FFFFFF" d="M2495.83,791.67v50h50v-50H2495.83z M2537.5,833.33h-33.33V800h33.33V833.33z"/>
        <path id="path3265_2_" fill="#93278F" d="M2529.94,760.24c-0.04-4.79,6.24-4.85,6.28,0l-0.11,14.58l3.39-4.6
            c2.32-2.81,6.42,1.13,3.87,4.32l-8.17,10.78c-1.28,1.43-2.97,1.42-4.24,0l-8.17-10.82c-2.51-3.19,1.62-7.18,3.9-4.32l3.36,4.64
            L2529.94,760.24"/>
        <path id="path3267_2_" fill="#93278F" d="M2507.16,782.81c-0.04,4.79,6.23,4.84,6.27,0l-0.11-14.58l3.39,4.56
            c2.32,2.85,6.42-1.1,3.91-4.28l-8.21-10.78c-1.29-1.44-2.96-1.43-4.24,0l-8.17,10.82c-2.51,3.18,1.62,7.17,3.91,4.28l3.36-4.6
            L2507.16,782.81"/>
        <path id="path3269_2_" fill="#93278F" d="M2536.58,816.29c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.34,0
            c-0.57,0-1.11-0.51-1.09-1.25v-8.81c-0.02-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V816.29"
            />
        <path id="path3271_2_" fill="#93278F" d="M2534.79,817.21h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V817.21"/>
        <path id="path3273_2_" fill="#93278F" d="M2531.58,817.21h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V817.21"/>
        <path id="path3275_2_" fill="#93278F" d="M2531.83,805.06c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.17-0.01-2.11,1.03-2.11,2.3C2529.72,804.01,2530.66,805.05,2531.83,805.06"/>
        <path id="path3277_2_" fill="#93278F" d="M2526.54,816.29c0,0.75-0.53,1.25-1.14,1.25c0,0-7.32,0-7.33,0
            c-0.57,0-1.11-0.51-1.09-1.25v-8.81c-0.01-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.49,0,6.49,0c0.87-0.02,1.53,0.75,1.53,1.7V816.29
            "/>
        <path id="path3279_2_" fill="#93278F" d="M2524.76,817.21h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V817.21"/>
        <path id="path3281_2_" fill="#93278F" d="M2521.54,817.21h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V817.21"/>
        <path id="path3283_2_" fill="#93278F" d="M2521.8,805.06c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.11,1.03-2.12,2.3C2519.69,804.01,2520.62,805.05,2521.8,805.06"/>
        <path id="path3285_2_" fill="#93278F" d="M2516.5,816.29c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0
            c-0.57,0-1.11-0.51-1.1-1.25v-8.81c-0.02-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V816.29"/>
        <path id="path3287_2_" fill="#93278F" d="M2514.72,817.21h-2.81v11.95c0.01,2.11,2.81,2.11,2.81,0V817.21"/>
        <path id="path3289_2_" fill="#93278F" d="M2511.5,817.21h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V817.21"/>
        <path id="path3291_2_" fill="#93278F" d="M2511.76,805.06c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.12,1.03-2.12,2.3C2509.64,804.01,2510.58,805.05,2511.76,805.06"/>
    </g>
    <g transform="scale(0.24,0.24)">
        <path id="path3261_3_" display="none" fill="#93278F" d="M3239.44,829.86V793.2c-0.01-3.66,2.53-6.7,6.02-6.7h33.25
            c3.37,0,6.02,3,6.02,6.7v36.71c0.01,3.88-2.69,6.62-5.99,6.62h-33.28l0,0C3242.16,836.53,3239.42,833.62,3239.44,829.86"/>
        <path fill="#FFFFFF" d="M3237.5,787.5v50h50v-50H3237.5z M3279.17,829.17h-33.33v-33.33h33.33V829.17z"/>
        <path id="path3265_3_" fill="#93278F" d="M3270.28,758.11c-0.04-4.79,6.24-4.85,6.28,0l-0.11,14.58l3.39-4.6
            c2.32-2.81,6.42,1.13,3.87,4.32l-8.17,10.78c-1.28,1.43-2.97,1.42-4.24,0l-8.17-10.82c-2.51-3.19,1.62-7.18,3.9-4.32l3.36,4.64
            L3270.28,758.11"/>
        <path id="path3267_3_" fill="#93278F" d="M3247.5,780.68c-0.04,4.79,6.23,4.84,6.27,0l-0.11-14.58l3.39,4.56
            c2.32,2.85,6.42-1.1,3.91-4.28l-8.21-10.78c-1.29-1.44-2.96-1.43-4.24,0l-8.17,10.82c-2.51,3.18,1.62,7.17,3.91,4.28l3.36-4.6
            L3247.5,780.68"/>
        <path id="path3269_3_" fill="#93278F" d="M3276.92,814.16c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.34,0
            c-0.57,0-1.11-0.51-1.09-1.25v-8.81c-0.02-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814.16"
            />
        <path id="path3271_3_" fill="#93278F" d="M3275.13,815.09h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V815.09"/>
        <path id="path3273_3_" fill="#93278F" d="M3271.92,815.09h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V815.09"/>
        <path id="path3275_3_" fill="#93278F" d="M3272.17,802.93c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.17-0.01-2.11,1.03-2.11,2.3C3270.06,801.88,3271,802.92,3272.17,802.93"/>
        <path id="path3277_3_" fill="#93278F" d="M3266.88,814.16c0,0.75-0.53,1.25-1.14,1.25c0,0-7.32,0-7.33,0
            c-0.57,0-1.11-0.51-1.09-1.25v-8.81c-0.01-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.49,0,6.49,0c0.87-0.02,1.53,0.75,1.53,1.7V814.16
            "/>
        <path id="path3279_3_" fill="#93278F" d="M3265.1,815.09h-2.81v11.95c0.01,2.11,2.8,2.11,2.81,0V815.09"/>
        <path id="path3281_3_" fill="#93278F" d="M3261.88,815.09h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V815.09"/>
        <path id="path3283_3_" fill="#93278F" d="M3262.14,802.93c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.11,1.03-2.12,2.3C3260.03,801.88,3260.96,802.92,3262.14,802.93"/>
        <path id="path3285_3_" fill="#93278F" d="M3256.84,814.16c0,0.75-0.53,1.25-1.13,1.25c0,0-7.32,0-7.33,0
            c-0.57,0-1.11-0.51-1.1-1.25v-8.81c-0.02-1.02,0.72-1.71,1.53-1.7c-0.02-0.02,6.5,0,6.5,0c0.87-0.02,1.53,0.75,1.53,1.7V814.16"/>
        <path id="path3287_3_" fill="#93278F" d="M3255.06,815.09h-2.81v11.95c0.01,2.11,2.81,2.11,2.81,0V815.09"/>
        <path id="path3289_3_" fill="#93278F" d="M3251.84,815.09h-2.81v11.95c0,2.09,2.8,2.09,2.81,0V815.09"/>
        <path id="path3291_3_" fill="#93278F" d="M3252.1,802.93c1.14-0.01,2.08-1.05,2.08-2.34c0-1.27-0.94-2.31-2.08-2.3
            c-1.18-0.01-2.12,1.03-2.12,2.3C3249.98,801.88,3250.92,802.92,3252.1,802.93"/>
    </g>