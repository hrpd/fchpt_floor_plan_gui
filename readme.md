## FCHPT Floor Plan - GUI

The goal of this project is a creation of web application that will be used as an interactive Floor plan of Faculty of Chemical and Food Technology. In addition it will offer timetable data. Application is meant to make contacting with teachers easier by providing their current location information based on their personal timetable. This part is mainly focused on graphical user interface creation with the use of various web technologies. The backend part of this application can be found on the [FCHPT-API](https://bitbucket.org/hrpd/fchpt_api) repository.

## Server requirements
- PHP >= 5.4
- MCrypt PHP Extension
- Composer

## Installation - Production
1. Clone this repoitory.
2. Run `composer install —no-dev`.
3. Set up your server to point to `public` directory.
4. Access the page. NOTE: Initial load can be longer due to initial caching

## Installation - Production With Tests
1. Clone this repoitory.
2. Run `composer install`.
3. Set up your server to point to `public` directory.
4. Set up correct `url` in `app/config/app.php`
4. Set up correct `base_url` in `behat.yml`.
4. Run the tests : (While in app root directory…)
	- `vendor/bin/codecept run unit` for unit tests
	- `vendor/bin/behat` for integration tests
5. Access the page. NOTE: Initial load can be longer due to initial caching

### Optional
- Change cache driver in `app/config/cache.php`. Default is `file`.
- Change path to Backend API in `app/Floorplan/Repositories/APIRoomRepository` by changing variable `$api_url`.
- `Vargantfile` with `vagrant-install.sh` custom provisioning script is provided. Tutorial on how to set up Vagrant can be found on [Vagrant Docs](http://docs.vagrantup.com/v2/getting-started/index.html). Running application using Vagrant will need no url variables editing mentioned in installation part.